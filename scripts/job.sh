#!/bin/bash

id='wobble2'

#time didgmo scan_conical $id 2.0 6 0.04 0.12 0.04 0.01 rank_rel_sum 60 90 0.1
#time didgmo scan_segmental $id 2.0 6 0.04 0.12 0.10 0.10 rank_rel_sum 60 90 1.0
#time didgmo scan_evolution evo1 2.0 6 0.04 0.12 0.10 0.10 0.02 0.005 rank_rel_sum 60 90 0.1
time didgmo scan_evolution $id 2.0 8 0.04 0.12 0.10 0.10 0.02 0.005 rank_wobble 60 90 15 5

./plot.sh geo2fft $id
evince $id.eps

