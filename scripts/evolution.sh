#!/bin/bash

id=$1			# name
l0=1.99		# minimal length
l1=2.01		# maximal length
ns=$2			# number of segments
np=$3			# size of evolution pool
ni=$4			# number of evolution iterations
d0=0.04		# mouth diameter
d1=0.11		# bell end diameter
mf=0.02		# mutation factor
s0=0.02		# minimal segment length
si=0.20		# segment increment size
db=0.02		# diameter band
di=0.005	# diameter increment size
f0=60			# minimal frequency
f1=90			# maximal frequency
ps=3			# number of peaks to optimize

time didgmo scan_evolution $id $l0 $l1 $ns $np $ni $d0 $d1 $mf $s0 $si $db $di rank_lua harmonic.lua $f0 $f1 $ps

