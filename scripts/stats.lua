PLUGIN_INFO = {
	name = 'stats.lua',
	description = 'outputs some statistics of given modeling project',
	num_params = 1,
	types = {
		T_STRING
	},
	defs = {
		'name of modeling project'
	}
}

function init (name)
	print ('init (' .. name .. ')')
end

function deinit ()
	print 'deinit'
end

function run (ptr)
	print 'run'

	return 0 -- successful execution
end

