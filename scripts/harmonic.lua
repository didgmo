should = {
	min = tonumber (arg[1]),	-- minimal frequency of ground tone
	max = tonumber (arg[2]),	-- maximal frequency of ground tone
	num = tonumber (arg[3])		-- desired number of peaks with high amplitude
}

if should.num < 2 then should.num = 2 end

function rank (peak)
	if (peak.max_freq[1] < should.min) or (peak.max_freq[1] > should.max) then
		res = 1e4
	else
		res = 1e5
	end

	rel = {}
	for i, v in ipairs (peak.max_amp) do
		rel[i] = v / peak.max_amp[1]
	end

	table.sort (rel)

	for i = 2, should.num do
		res = res * rel[i]
	end

	return res
end

