#!/bin/bash

case "$1" in
	fft)
		echo "call 'fft.gpi' '$2' '$3'" | gnuplot -persist
	;;

	geo2fft)
		gawk_peak=`tempfile`
		gawk '{print "set label \"" $5 "\" at (" $4 "),(20*log10(" $6 "*2e-5)) center front offset (0),(2) textcolor ls 3"}' $2.peak > $gawk_peak
		echo "call 'alternate.gpi' '$2' '$gawk_peak'" | gnuplot -persist
		rm $gawk_peak
	;;

	trans)
		gawk_peak=`tempfile`
		gawk '{print "set label \"" $5 "\" at (" $4 "),(20*log10(" $6 "*2e-5)) center front offset (0),(2) textcolor ls 7"}' $2.peak > $gawk_peak
		echo "call 'transparent.gpi' '$2' '$gawk_peak'" | ./gnuplot-4.3 -persist
		rm $gawk_peak
	;;
esac


