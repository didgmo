#ifndef DIDGMO_H
#define DIDGMO_H

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <glib.h>
#include <gmodule.h>

/*
 * typedefs
 */

typedef struct _fft fft;

typedef GQueue geo;
typedef struct _seg seg;

typedef struct _peak peak;

typedef struct _wave wave;

typedef struct _plugin plugin;
typedef enum _plugin_param_type plugin_param_type;
typedef struct _plugin_info plugin_info;
typedef struct gchar *plugin_param_def;
typedef struct _plugin_param plugin_param;
typedef struct _plugin_return plugin_return;

typedef plugin_info *(* Tfunc_info) ();
typedef void (* Tfunc_init) (int, char *);
typedef void (* Tfunc_deinit) ();
typedef plugin_return *(* Tfunc_run) (void *);

/*
 * cadsd
 */

double cadsd_Ze (geo *, double);

/*
 * fft
 */

struct _fft {
	int size;
	double max;
	double step;
	double *imp;
	double *snd;
	double *ovr;
};

fft *fft_new (double, double);
void fft_free (fft *);

wave *fft_wave (fft *, int);
fft *fft_dB (fft *);

void fft_save (fft *, char *);
fft *fft_load (char *);

peak *fft_peak (fft *, int);

/*
 * geo
 */

struct _seg {
	double d0, d1, L;
	double phi, a0, a01, a1, l, x1, x0, r0;
};

seg *seg_new (double, double, double);
void seg_update (seg *);
seg *seg_copy (seg *);
void seg_free (seg *);

geo *geo_new ();
void geo_free (geo *);

void geo_push_head (geo *, seg *);
void geo_push_tail (geo *, seg *);
seg *geo_pop_head (geo *);
seg *geo_pop_tail (geo *);

int geo_get_length (geo *);
double geo_length_get (geo *);
seg *geo_get_nth (geo *, int);

geo *eog_load (char *);
geo *geo_load (char *);
geo *geo_copy (geo *);
void geo_save (geo *, char *);
void geo_eog (geo *, char *);

fft *geo_fft (geo *, double, double, int);

/*
 * peak
 */

struct _peak {
	int max_size, min_size;
	double *max_freq, *min_freq;
	double *max_amp, *min_amp;
};

peak *peak_new (int, int);
void peak_free (peak *);

void peak_save (peak *, char *);
void peak_label (peak *, char *);
peak *peak_load (char *);

char *peak_tone (int);

/*
 * plugin
 */

struct _plugin {
	GModule *module;
	Tfunc_info info;
	Tfunc_init init;
	Tfunc_deinit deinit;
	Tfunc_run run;
};

enum _plugin_param_type {T_STRING, T_INT, T_FLOAT, T_PLUGIN, T_STATUS, T_FFT, T_GEO, T_WAVE, T_PEAK};

struct _plugin_param {
	gchar *d_string;
	gint d_int;
	gdouble d_float;
	gchar *d_plugin;
	gpointer d_pointer;
};

struct _plugin_info {
	gchar *name;
	gchar *description;
	gint param_number;
	plugin_param_type *types;
	plugin_param_def *definitions;
	plugin_param *params;
};

struct _plugin_return {
	gint param_number;
	plugin_param_type *types;
	plugin_param *params;
};

#define PLUGIN_REGISTER \
plugin_info *\
__info ()\
{\
	return &PLUGIN_INFO;\
}

#define PLUGIN_PARSE \
plugin_parse (&PLUGIN_INFO, argc, argv);

plugin *plugin_new (char *);
void plugin_free (plugin *);
void plugin_parse (plugin_info *, int, char **);

plugin_info *plugin_get_info (plugin *);
void plugin_init (plugin *, int, char **);
void plugin_deinit (plugin *);
void *plugin_run (plugin *, void *);

void plugin_info_print (plugin_info *);

plugin_return *plugin_return_new (int);
void plugin_return_free (plugin_return *);

/*
 * wave
 */

struct _wave {
	int size;
	int rate;
	double *array;
};

wave *wave_new (int);
void wave_free (wave *);

wave *wave_load (char *, int);
void wave_save (wave *, char *);

fft *wave_fft (wave *, double);
void wave_play (wave *);

#endif /* DIDGMO_H */

