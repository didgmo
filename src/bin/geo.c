/** \defgroup geo geo
 *
 * Routines for defining, loading, saving of the didgeridoo geometry.
 * \ingroup didgmo
 */

#include <math.h>

#include "didgmo.h"

const double p = 1.2929, n = 1.708e-5, c = 331.45 * sqrt (293.16 / 273.16);
const double PI = (double) M_PI;

/** updates private data of a segment.
 *
 * @param i_seg segment to be updated
 *
 * \ingroup geo
 */
void
seg_update (seg *i_seg)
{
	double i_d0 = i_seg->d0;
	double i_d1 = i_seg->d1;
	double i_L = i_seg->L;

	double dp = i_d1 + i_d0;
	double dm = i_d1 - i_d0;
   
	i_seg->a0 = PI * i_d0 * i_d0 / 4;
	i_seg->a01 = PI * dp * dp / 16.0;
	i_seg->a1 = PI * i_d1 * i_d1 / 4;
  i_seg->phi = atan (dm / (2 * i_L));

	double sin_phi = sin (i_seg->phi);

  i_seg->l = dm / (2 * sin_phi);
  i_seg->x1 = i_d1 / (2 * sin_phi);
  i_seg->x0 = i_seg->x1 - i_seg->l;
  i_seg->r0 = p * c / i_seg->a0;
}

/** creates new segment.
 *
 * @param i_L length of segment
 * @param i_d0 diameter at beginning of segment
 * @param i_d1 diameter at end of segment
 *
 * @return a new segment (free it with seg_free)
 *
 * \ingroup geo
 */
seg *
seg_new (double i_L, double i_d0, double i_d1)
{
	seg *o_seg = g_new (seg, 1);
	o_seg->L = i_L;
	o_seg->d0 = i_d0;
	o_seg->d1 = i_d1;

	seg_update (o_seg);

	return o_seg;
}

/** copies a segment.
 *
 * @param i_seg segment to be copied
 *
 * @return a new segment (free it with seg_free)
 *
 * \ingroup geo
 */
seg *
seg_copy (seg *i_seg)
{
	seg *o_seg = g_new (seg, 1);

	o_seg->L = i_seg->L;
	o_seg->d0 = i_seg->d0;
	o_seg->d1 = i_seg->d1;

	o_seg->a0 = i_seg->a0;
	o_seg->a01 = i_seg->a01;
	o_seg->a1 = i_seg->a1;
	o_seg->phi = i_seg->phi;

	o_seg->l = i_seg->l;
	o_seg->x1 = i_seg->x1;
	o_seg->x0 = i_seg->x0;
	o_seg->r0 = i_seg->r0;

	return o_seg;
}

/** frees segment created with seg_new.
 *
 * @param i_seg segment to be freed
 *
 * \ingroup geo
 */
void
seg_free (seg *i_seg)
{
	g_free (i_seg);
}

/** creates new geometry structure.
 *
 * @return geometry (free it with geo_free)
 *
 * \ingroup geo
 */
geo *
geo_new ()
{
	return g_queue_new ();
}

/** frees geometry created with geo_new.
 *
 * @param i_geo geometry to be freed
 *
 * routine frees automatically all associated segments.
 *
 * \ingroup geo
 */
void
geo_free (geo* i_geo)
{
	seg *i_seg;
	while (i_seg = g_queue_pop_tail (i_geo))
		seg_free (i_seg);
	g_queue_free (i_geo);
}

/** push new segment at head of geometry.
 *
 * @param i_geo geometry
 * @param i_seg segemet to be added to geometry
 *
 * \ingroup geo
 */
void
geo_push_head (geo *i_geo, seg *i_seg)
{
	g_queue_push_head (i_geo, i_seg);
}

/** push new segment at tail of geometry.
 *
 * @param i_geo geometry
 * @param i_seg segment to be added to geometry
 *
 * \ingroup geo
 */
void
geo_push_tail (geo *i_geo, seg *i_seg)
{
	g_queue_push_tail (i_geo, i_seg);
}

/** pop segment from head of geometry.
 *
 * @param i_geo geometry
 *
 * @return segment
 *
 * \ingroup geo
 */
seg *
geo_pop_head (geo *i_geo)
{
	return (seg *) g_queue_pop_head (i_geo);
}

/** pop segment from tail of geometry.
 *
 * @param i_geo geometry
 *
 * @return segmnet
 *
 * \ingroup geo
 */
seg *
geo_pop_tail (geo *i_geo)
{
	return (seg *) g_queue_pop_tail (i_geo);
}

/** get number of segment of geometry.
 *
 * @param i_geo geometry
 *
 * @return number of segments
 *
 * \ingroup geo
 */
int
geo_get_length (geo *i_geo)
{
	return g_queue_get_length (i_geo);
}

void
_length (gpointer data, gpointer user_data)
{
	*((double *)user_data) += ((seg *)data)->L;
}

/** get length of final didgeridoo represented by geometry.
 *
 * @param i_geo geometry
 *
 * @return sum of length of all segments
 *
 * \ingroup geo
 */
double
geo_length_get (geo *i_geo)
{
	double res = 0;
	g_queue_foreach (i_geo, _length, &res);
	return res;
}

/** get n_th segment of geometry.
 *
 * @param i_geo geometry
 * @param i_index index of n_th segment
 *
 * @return n_th segment
 *
 * \ingroup geo
 */
seg *
geo_get_nth (geo *i_geo, int i_index)
{
	return (seg *) g_queue_peek_nth (i_geo, i_index);
}

/** load geometry (*.geo) from file.
 *
 * @param i_path file path
 *
 * @return geometry
 *
 * \ingroup geo
 */
geo *
geo_load (char *i_path)
{
	geo *o_geo = geo_new ();
	FILE *i_file = fopen (i_path, "r");
	while (!feof (i_file))
	{
		double l, d0, d1;
		fscanf (i_file, "%lf %lf %lf\n", &l, &d0, &d1);
		//printf ("%f %f %f\n", l, d0, d1);
		geo_push_tail (o_geo, seg_new (l, d0, d1));
	}
	fclose (i_file);
	return o_geo;
}

/** load geometry (*.eog) from file.
 *
 * @param i_path file path
 *
 * @return geometry
 *
 * \ingroup geo
 */
geo *
eog_load (char *i_path)
{
	geo *o_geo = geo_new ();
	FILE *i_file = fopen (i_path, "r");
	double l_old, l_new, d_old, d_new;
	while (!feof (i_file))
	{
		fscanf (i_file, "%lf %lf\n", &l_new, &d_new);		
		if (l_new > 0)
			geo_push_tail (o_geo, seg_new (l_new - l_old, d_old, d_new));
		l_old = l_new;
		d_old = d_new;
	}
	fclose (i_file);
	return o_geo;
}

static void
geo_copy_func (seg *i_seg, geo *o_geo)
{
	seg *o_seg = seg_copy (i_seg);
	g_queue_push_tail (o_geo, o_seg);
}

/** copy a geometry.
 *
 * @param i_geo geometry to copy
 *
 * @return new geometry (free with geo_free)
 *
 * \ingroup geo
 */
geo *
geo_copy (geo *i_geo)
{
	geo *o_geo = geo_new ();
	g_queue_foreach (i_geo, geo_copy_func, o_geo);
	return o_geo;
}

void
_save (gpointer data, gpointer user_data)
{
	seg *i_seg = (seg *) data;
	FILE *file = (FILE *) user_data;
	fprintf (file, "%f %f %f\n", (double) i_seg->L, (double) i_seg->d0, (double) i_seg->d1);
}

/** save geometry to file.
 *
 * @param i_geo geometry
 * @param i_path file path
 *
 * \ingroup geo
 */
void
geo_save (geo *i_geo, char *i_path)
{
	FILE *file = fopen (i_path, "w");
	g_queue_foreach (i_geo, _save, file);
	fclose (file);
}

double tmp_len;

void
_eog (gpointer data, gpointer user_data)
{
	seg *i_seg = (seg *) data;
	FILE *file = (FILE *) user_data;
	tmp_len += i_seg->L;
	fprintf (file, "%f %f\n", tmp_len, (double) i_seg->d1);
}

/* save geometry to file (alternative form).
 *
 * @param i_geo geometry
 * @param i_path file path
 *
 * \ingroup geo
 */
void
geo_eog (geo *i_geo, char *i_path)
{
	FILE *file = fopen (i_path, "w");
	tmp_len = 0.0;
	seg *head = (seg *) g_queue_peek_head (i_geo);
	fprintf (file, "%f %f\n", 0.0, (double) head->d0);
	g_queue_foreach (i_geo, _eog, file);
	fclose (file);
}

/** calculate whole impedance, sound and overblow spectra of given geometry.
 *
 * @param i_geo geometry
 * @param i_max frequency range from 0 to i_max
 * @param i_offset frequency offset of ground tone and first overblow
 *
 * @return fft spectra
 *
 * \ingroup geo
 */
fft *
geo_fft (geo *i_geo, double i_max, double i_step, int i_offset)
{
	fft *o_fft = fft_new (i_max, i_step);
	const int i_size = o_fft->size;
	o_fft->imp[0] = 0;
	o_fft->ovr[0] = 0;
	int i, j;
	for (i = 1; i < i_size; i++)
	{
		double I = (double) i * i_max / (double) i_size;
		o_fft->imp[i] = cadsd_Ze (i_geo, I);
		o_fft->snd[i] = 0;
		o_fft->ovr[i] = 0;
	}

	// search for peaks and valleys
	int peaks [2];
	int vally [2];
  int up = 0, npeaks = 0, nvally = 0;
  for (i = 2; i < i_size; i++) {
    if (o_fft->imp[i] > o_fft->imp[i-1])
		{
			if (npeaks && !up)
			{
				vally [nvally] = i - 1;
				nvally++;
			}
			up = 1;
		}
    else
		{
    	if (up) {
      	peaks[npeaks] = i - 1;
				npeaks++;
    	}
			up = 0;
		}
		if (nvally > 1)
			break;
  }
	//printf ("%d %d %d %d\n", peaks[0], vally[0], peaks[1], vally[1]);

	const double k = 0.0001;
	int mem0 = peaks[0];
	int mem0a = peaks[0];
	//int mem0b = vally[0] - peaks[0];
	int mem0b = mem0a;
  // calculate overblow spectrum of base tone
  for (i = mem0; i < i_size; i += mem0)
    for (j = -mem0a; j < mem0b; j++)
		{
			if (i+j < i_size)
			{
				double I = (double) (i+j) * i_max / (double) i_size;
				// printf ("%d\n", i + j);
				if (j < 0)
					o_fft->snd[i+j+i_offset] += o_fft->imp[mem0+j] * exp (I * k);
				else
					o_fft->snd[i+j+i_offset] += o_fft->imp[mem0-j] * exp (I * k);
			}
		}
 
	// calculate sound specturm of base tone
  for (i = 0; i < i_size; i++)
		o_fft->snd[i] = o_fft->imp[i] * o_fft->snd[i] * 1e-6;

	int mem1 = peaks[1];
	int mem1a = peaks[1] - vally[0];
	//int mem1b = vally[1] - peaks[1];
	int mem1b = mem1a;
	// calculate overblow spectrum of first overblow
	for (i = mem1; i < i_size; i += mem1)
		for (j = -mem1a; j < mem1b; j++)
		{
			if (i+j < i_size)
			{
				double I = (double) (i+j) * i_max / (double) i_size;
				if (j < 0)
					o_fft->ovr[i+j+i_offset] += o_fft->imp[mem1+j] * exp (I * k);
				else
					o_fft->ovr[i+j+i_offset] += o_fft->imp[mem1-j] * exp (I * k);
			}
		}
	
	// calculate sound spectrum of first overblow
	for (i = 0; i < i_size; i++)
		o_fft->ovr[i] = o_fft->imp[i] * o_fft->ovr[i] * 1e-6;

	return o_fft;
}

