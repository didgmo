/** \defgroup wave wave 
 *
 * Routines for interaction with wave files.
 * \ingroup didgmo
 */

#include <math.h>
#include <fftw3.h>
#include <sndfile.h>

#include "didgmo.h"

/** creates new wave object with size i_size.
 *
 * @param i_size size of wave array (bitrate x duration)
 *
 * @return wave object (free with wave_free)
 *
 * \ingroup wave
 */
wave *
wave_new (int i_size)
{
	wave *o_wave = (wave *) g_malloc (sizeof (wave));
	o_wave->size = i_size;
	o_wave->array = (double *) g_malloc (sizeof (double) * i_size);
	return o_wave;
}

/** frees wave object.
 *
 * @param i_wave wave object
 *
 * \ingroup wave
 */
void
wave_free (wave *i_wave)
{
	free (i_wave->array);
	free (i_wave);
}

/** loads given wave file.
 *
 * @param i_path file path
 * @param i_sec duration to read from wave file
 *
 * @return wave object
 *
 * \ingroup wave
 */
wave *
wave_load (char *i_path, int i_sec)
{
	SF_INFO sfinfo;
	sfinfo.format = 0;
	SNDFILE *sndfile = sf_open (i_path, SFM_READ, &sfinfo);

	printf ("%s %d %d %d %d %d %d\n",
		i_path,
		sfinfo.frames,
		sfinfo.samplerate,
		sfinfo.channels,
		sfinfo.format,
		sfinfo.sections,
		sfinfo.seekable);

	int length = sfinfo.samplerate * i_sec;
	wave *o_wave = wave_new (length);
	sf_read_double (sndfile, o_wave->array, o_wave->size);
	o_wave->rate = sfinfo.samplerate;

	sf_close (sndfile);

	return o_wave;
}

/** saves given wave file.
 *
 * @param i_wave wave object
 * @param i_path file path
 *
 * \ingroup wave
 */
void
wave_save (wave *i_wave, char *i_path)
{
	SF_INFO sfinfo;
	sfinfo.samplerate = 44100;
	sfinfo.channels = 1;
	sfinfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16; //SF_FORMAT_DOUBLE;

	SNDFILE *sndfile = sf_open (i_path, SFM_WRITE, &sfinfo);
	sf_write_double (sndfile, i_wave->array, i_wave->size);

	printf ("%s %d %d %d %d %d %d\n",
		i_path,
		sfinfo.frames,
		sfinfo.samplerate,
		sfinfo.channels,
		sfinfo.format,
		sfinfo.sections,
		sfinfo.seekable);

	sf_close (sndfile);

	int i;
	for (i = 0; i < i_wave->size; i++)
		printf ("%d\n", i_wave->array[i]);
}

/** fast fourier transformation of wave object to a fft spectrum.
 *
 * @param i_wave wave object
 *
 * @return fft spectrum (only sound spectrum is defined)
 *
 * \ingroup wave
 */
fft *
wave_fft (wave *i_wave, double i_freq)
{
	const int n = i_wave->size;
  int i;

  static double *rdata = NULL,*idata = NULL;
  static fftw_plan rplan;

  rdata = (double *) fftw_malloc (n * sizeof (double));
	idata = (double *) fftw_malloc (n * sizeof (double));
  rplan = fftw_plan_r2r_1d(n, rdata, idata, FFTW_DHT, FFTW_ESTIMATE);
	for (i = 0; i < n; i++)
		idata[i] = 0;
	//memset ((void *) idata, 0, n * sizeof (double));
	for (i = 0; i < n; i++)
		rdata[i] = i_wave->array[i];
	fftw_execute(rplan);
  
	fft *o_fft = fft_new (i_freq, (double) i_wave->rate / i_wave->size);
	for (i = 0; i < o_fft->size; i++)
		o_fft->ovr[i] = abs (idata[i]);
        
	fftw_destroy_plan(rplan);
	fftw_free(rdata);
	fftw_free(idata);

	return o_fft;
}

// TODO test it
#include <fcntl.h>
#include <sys/soundcard.h>
#include <stdlib.h>

static void
write_wave (int fd_out, short buf[])
{
	if (write (fd_out, buf, sizeof (buf)) != sizeof (buf))
	{
		perror ("Audio write");
		exit (-1);
	}
}

static int
open_audio_device (char *name, int mode)
{
	int tmp, fd;

	if ((fd = open (name, mode, 0)) == -1)
	{
		perror (name);
		exit (-1);
	}

	tmp = AFMT_S16_NE;		/* Native 16 bits */
	if (ioctl (fd, SNDCTL_DSP_SETFMT, &tmp) == -1)
	{
		perror ("SNDCTL_DSP_SETFMT");
		exit (-1);
	}

	if (tmp != AFMT_S16_NE)
	{
		fprintf (stderr,
			 "The device doesn't support the 16 bit sample format.\n");
		exit (-1);
	}

	tmp = 2;
	if (ioctl (fd, SNDCTL_DSP_CHANNELS, &tmp) == -1)
	{
		perror ("SNDCTL_DSP_CHANNELS");
		exit (-1);
	}

	if (tmp != 2)
	{
		fprintf (stderr, "The device doesn't support stereo mode.\n");
		exit (-1);
	}

	int sample_rate = 44100;
	if (ioctl (fd, SNDCTL_DSP_SPEED, &sample_rate) == -1)
	{
		perror ("SNDCTL_DSP_SPEED");
		exit (-1);
	}

	return fd;
}

/** play back wave object on sound card.
 *
 * @param i_wave wave object
 *
 * \ingroup wave
 */
void
wave_play (wave *i_wave)
{
	int fd_out = open_audio_device ("/dev/dsp", O_WRONLY);
	write_wave (fd_out, (short *) i_wave->array);
}

