#include <complex.h>
#include <math.h>

#include "didgmo.h"

extern const double p, n, c;
extern const double PI;

typedef struct _matrix matrix;

struct _matrix {
	double complex x[2][2];
};

matrix
dot (matrix m1, matrix m2)
{
	matrix tmp;
	tmp.x[0][0] = m1.x[0][0]*m2.x[0][0] + m1.x[0][1]*m2.x[1][0];
	tmp.x[0][1] = m1.x[0][0]*m2.x[0][1] + m1.x[0][1]*m2.x[1][1];
	tmp.x[1][0] = m1.x[1][0]*m2.x[0][0] + m1.x[1][1]*m2.x[1][0];
	tmp.x[1][1] = m1.x[1][0]*m2.x[0][1] + m1.x[1][1]*m2.x[1][1];
	return tmp;
}

matrix
inv (matrix m)
{
  matrix tmp;
  double complex a,b,c,d,x;
  
  a = m.x[0][0];
  b = m.x[0][1];
  c = m.x[1][0];
  d = m.x[1][1];
  x = a*d - b*c;
  
  tmp.x[0][0] =  d/x;
  tmp.x[0][1] = -b/x;
  tmp.x[1][0] = -c/x;
  tmp.x[1][1] =  a/x;
  return tmp;
}

double L, d0, d1, a0, a01, a1, l, x0, x1, r0;
double complex Tw, Zcw, Tw_x0, Tw_x1, Tw_l, ccosh_Tw_l, csinh_Tw_l;
matrix tmp;

inline double
w (double f)
{
	return 2.0 * PI * (double) f;
}

inline double
k (double w)
{
	return w / c;
}

inline double
rv (double w)
{
	return sqrt (p * w * a01 / (n * PI));
}

inline double complex
T (double w)
{
	double kw = k (w);
	double rvw = rv (w);
  return kw * 1.045 / rvw + kw * (1.0 + 1.045 / rvw) * I;
}

inline double complex
Zc (double w)
{
	double rvw = rv (w);
	return r0 * (1.0 + 0.369 / rvw) - r0 * 0.369 / rvw * I;
}

inline double complex
a11_cone ()
{
  return x1 / x0 * ccosh_Tw_l - csinh_Tw_l / Tw_x1;
}

inline double complex
a11_cyl ()
{
	return ccosh_Tw_l;
}

inline double complex
a12_cone ()
{
	return x0 / x1 * Zcw * csinh_Tw_l;
}

inline double complex
a12_cyl ()
{
	return Zcw * csinh_Tw_l;
}

inline double complex
a21_cone ()
{
	return ((x1 / x0 - 1.0 / (Tw_x0 * Tw_x0)) * csinh_Tw_l + Tw_l / (Tw_x0 * Tw_x0) * ccosh_Tw_l) / Zcw;
}

inline double complex
a21_cyl ()
{
	return csinh_Tw_l / Zcw;
}

inline double complex
a22_cone ()
{
	return x0 / x1 * (ccosh_Tw_l + csinh_Tw_l / Tw_x0);
}

inline double complex
a22_cyl ()
{
	return ccosh_Tw_l;
}

matrix
a (double w)
{
  Tw = T (w);
  Zcw = Zc (w);

	Tw_x0 = Tw * x0;
	Tw_x1 = Tw * x1;
	Tw_l = Tw * l;
	ccosh_Tw_l = ccosh (Tw_l);
	csinh_Tw_l = csinh (Tw_l);

	matrix _tmp;
	if (d0 != d1)
	{
		_tmp.x[0][0] = a11_cone (w);
		_tmp.x[0][1] = a12_cone (w);
		_tmp.x[1][0] = a21_cone (w);
		_tmp.x[1][1] = a22_cone (w);
	}
	else
	{
		_tmp.x[0][0] = a11_cyl (w);
		_tmp.x[0][1] = a12_cyl (w);
		_tmp.x[1][0] = a21_cyl (w);
		_tmp.x[1][1] = a22_cyl (w);
	}
	return _tmp;
}

void b_ap
(gpointer data, gpointer user_data)
{
	seg *t_seg = (seg *) data;
	double w = *((double *) user_data);

 	L = t_seg->L;
	d0 = t_seg->d0;
	d1 = t_seg->d1;
	a0 = t_seg->a0;
	a01 = t_seg->a01;
	a1 = t_seg->a1;
	l = t_seg->l;
	x0 = t_seg->x0;
	x1 = t_seg->x1;
	r0 = t_seg->r0;

  tmp = dot (tmp, a (w));
}

matrix
ap (double w, geo *i_geo)
{
  tmp.x[0][0] = 1;
  tmp.x[0][1] = 0;
  tmp.x[1][0] = 0;
  tmp.x[1][1] = 1;

	//g_queue_reverse (i_geo);
	g_queue_foreach (i_geo, b_ap, &w);
  //rb_iterate(rb_each,self,b_ap,rb_float_new(w));

	return tmp;
}

double complex Za
(double w, geo *i_geo)
{
	seg *t_seg = (seg *) g_queue_peek_tail (i_geo);
 
 	L = t_seg->L;
	d0 = t_seg->d0;
	d1 = t_seg->d1;
	a0 = t_seg->a0;
	a01 = t_seg->a01;
	a1 = t_seg->a1;
	l = t_seg->l;
	x0 = t_seg->x0;
	x1 = t_seg->x1;
	r0 = t_seg->r0;

  double complex res = Zc(w)/2*(w*w*d1*d1/c/c + 0.6*L*w*d1/c*I); //from geipel
  return res;
}

double
cadsd_Ze (geo *i_geo, double i_f)
{
	double complex a = Za (w (i_f), i_geo);
	matrix b = ap (w (i_f), i_geo);
	double Ze = cabs ((a*b.x[0][0]+b.x[0][1])/(a*b.x[1][0]+b.x[1][1]));
	//printf ("Ze: %f\n", (double) Ze);
	return Ze;
}

