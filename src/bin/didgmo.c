/** \defgroup didgmo didgmo
 *
 * this is the source code documentation of didgmo. didgmo is programed in
 * plain ansi C in an object oriented way. the first implementation of didgmo
 * had a scripting support for Ruby, but then didgmo was redesigned for
 * a better speed performance. to allow the user to extend the program with
 * specific routines, didgmo makes use of plugins. the plugins are written in
 * plain ansi C, too. in order to implement your own routines, you need
 * to know about the structure of the program.
 *
 * this information is intended for programmers, who like to write their own
 * plugins.
 *
 * you actually only have to know about three classes: geo, fft, wave. the
 * geo class stores the didgeridoo geometry and includes routines for
 * loading and saving. the fft class stores the impedance and sound spectra
 * and includes routines for loading and saving. the wave object stores the
 * information of a sound file and includes routines for loading, saving and
 * playback. each class has additional routines, which allow for conversion
 * between the different objects, the most prominent beeing the function
 * geo_fft, which calculates the impedance and sound spectra for the given
 * geometry and returns them in an object of the class fft.
 */

/** \defgroup modules modules
 *
 * TODO
 */

#include "didgmo.h"

