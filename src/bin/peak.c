/** \defgroup peak peak
 *
 * Routines for interaction with peak objects.
 *
 * \ingroup didgmo
 */

#include "didgmo.h"

/** creates new peak object.
 *
 * @param i_min_size number of 'valleys'
 * @param i_max_size number of 'peaks'
 *
 * @return peak object (free with peak_free)
 *
 * \ingroup peak
 */
peak *
peak_new (int i_min_size, int i_max_size)
{
	peak *o_peak = g_new (peak, 1);
	o_peak->min_size = i_min_size;
	o_peak->max_size = i_max_size;
	o_peak->min_freq = g_new (double, i_min_size);
	o_peak->max_freq = g_new (double, i_max_size);
	o_peak->min_amp = g_new (double, i_min_size);
	o_peak->max_amp = g_new (double, i_max_size);
	return o_peak;
}

/** frees peak object.
 *
 * @param i_peak peak object
 *
 * \ingroup peak
 */
void
peak_free (peak *i_peak)
{
	g_free (i_peak->min_freq);
	g_free (i_peak->max_freq);
	g_free (i_peak->min_amp);
	g_free (i_peak->max_amp);
	g_free (i_peak);
}

/** saves peak object to file
 *
 * @param i_peak peak object
 * @param i_path file path
 *
 * \ingroup peak
 */
void
peak_save (peak *i_peak, char *i_path)
{
	int max;
	if (i_peak->min_size > i_peak->max_size)
		max = i_peak->min_size;
	else
		max = i_peak->max_size;

	FILE *o_file = fopen (i_path, "w");
	int i;
	for (i = 0; i < max; i++)
	{
		if (i < i_peak->min_size)
			fprintf (o_file, "%f %s %f\t", i_peak->min_freq[i], peak_tone (i_peak->min_freq[i]), i_peak->min_amp[i]);
		else
			fprintf (o_file, "na na na\t");

		if (i < i_peak->max_size)
			fprintf (o_file, "%f %s %f\n", i_peak->max_freq[i], peak_tone (i_peak->max_freq[i]), i_peak->max_amp[i]);
		else
			fprintf (o_file, "na na na\n");
	}
	fclose (o_file);
}

/** saves labels for gnuplot
 *
 * @param i_peak peak object
 * @param i_path file path
 */
void
peak_label (peak *i_peak, char *i_path)
{

	FILE *o_file = fopen (i_path, "w");
	int i;
	for (i = 0; i < i_peak->max_size; i++)
		fprintf (
			o_file,
			"set label \"%s\" at (%f),(20*log10(%f*2e-5)) center front offset (0),(2) textcolor ls 3\n",
			peak_tone (i_peak->max_freq[i]),
			i_peak->max_freq[i],
			i_peak->max_amp[i]);
	fclose (o_file);
}

/** load peak object from file
 *
 * @param i_path file path
 *
 * @return peak object (free with peak_free)
 *
 * \ingroup peak
 */
peak *
peak_load (char *i_path)
{
	// TODO
}

static char *tones [] = {
	"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" , "C"
};

static double freqs [] = {
	32.706,  34.650, 36.706, 38.894, 41.206, 43.656, 46.250, 40.000, 51.913, 55.000, 58.268, 61.737, 65.412
};

static double C0 = 32.706;

/** gives name of tone corresponding to frequency
 *
 * @param i_freq frequency
 *
 * @return name of tone
 *
 * \ingroup peak
 */
char *
peak_tone (int i_freq)
{
	int oct;
	for (oct = 5; oct >= 0; oct--)
	{
		double expo = pow (2.0, oct);
		double C = C0 * expo;
		if (i_freq > C)
		{
			double tone = ((double) i_freq) / expo;
			int k;
			for (k = 1; k < 13; k++)
				if (freqs[k] > tone)
					return tones[k - 1];
		}
	}
}

