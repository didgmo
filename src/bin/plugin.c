/** \defgroup plugin plugin
 *
 * Routines for interacting with the plugin system.
 *
 * \ingroup didgmo
 */

#include "didgmo.h"

/** creates new plugin instance.
 *
 * @param i_name name of plugin
 *
 * @return plugin instance (free with plugin_free)
 *
 * \ingroup plugin
 */
plugin *
plugin_new (char *i_name)
{
	char path[256];
#ifdef _WIN32
	sprintf (path, "%s/%s.dll", PKGMODULESDIR, i_name);
#else
	sprintf (path, "%s/%s.so", PKGMODULESDIR, i_name);
#endif

	plugin *i_plugin = g_new (plugin, 1);
	i_plugin->module = g_module_open (path, G_MODULE_BIND_LAZY);
	g_module_symbol (i_plugin->module, "__info", &i_plugin->info);
	g_module_symbol (i_plugin->module, "init", &i_plugin->init);
	g_module_symbol (i_plugin->module, "deinit", &i_plugin->deinit);
	g_module_symbol (i_plugin->module, "run", &i_plugin->run);
	return i_plugin;
}

/** frees plugin instance.
 *
 * @param i_plugin plugin instance
 *
 * \ingroup plugin
 */
void
plugin_free (plugin *i_plugin)
{
	g_module_close (i_plugin->module);
	g_free (i_plugin);
}

/** parses command line arguments
 *
 * @param i_plugin_info plugin info object
 * @param i_argc number of arguments
 * @param i_argv command line arguments
 *
 * \ingroup plugin
 */
void
plugin_parse (plugin_info *i_plugin_info, int i_argc, char **i_argv)
{
	if (i_argc < i_plugin_info->param_number)
	{
		fprintf (stderr, "too few arguments\n");
		plugin_info_print (i_plugin_info);
		exit (-1);
	}

	int i;
	for (i = 0; i < i_plugin_info->param_number; i++)
	{
		switch (i_plugin_info->types[i])
		{
			case T_STRING:
				assert (i_plugin_info->params[i].d_string = i_argv[i]);
				break;
			case T_INT:
				assert (i_plugin_info->params[i].d_int = atoi (i_argv[i]));
				break;
			case T_FLOAT:
				assert (i_plugin_info->params[i].d_float = atof (i_argv[i]));
				break;
			case T_PLUGIN:
				assert (i_plugin_info->params[i].d_plugin = i_argv[i]);
				break;
		}
	}
}

/** runs plugin info
 *
 * @param i_plugin plugin instance
 *
 * @return plugin info structure
 *
 * \ingroup plugin
 */
plugin_info *
plugin_get_info (plugin *i_plugin)
{
	return i_plugin->info ();
}

/** runs plugin init routine.
 *
 * @param i_plugin plugin instance
 * @param i_argc number of arguments
 * @param i_argv array of argmuments
 *
 * \ingroup plugin
 */
void
plugin_init (plugin *i_plugin, int i_argc, char **i_argv)
{
	i_plugin->init (i_argc, i_argv);
}

/** runs plugin deinit routine.
 *
 * \ingroup plugin
 */
void
plugin_deinit (plugin *i_plugin)
{
	i_plugin->deinit ();
}

/** runs plugin main routine.
 *
 * @param i_plugin plugin instance
 * @param i_data user data
 *
 * \ingroup plugin
 */
void *
plugin_run (plugin *i_plugin, void *i_data)
{
	return i_plugin->run (i_data);
}

static char *
plugin_type2string (plugin_param_type i_type)
{
	switch (i_type)
	{
		case T_INT:
			return "integer";
		case T_FLOAT:
			return "float";
		case T_STRING:
			return "string";
		case T_PLUGIN:
			return "plugin";
	}
}

/** print plugin information to stdout
 *
 * @param i_plugin_info plugin_info instance
 *
 * \ingroup plugin
 */
void
plugin_info_print (plugin_info *i_plugin_info)
{
	gchar *name;
	gchar *description;
	gint param_number;
	plugin_param_type *types;
	plugin_param_def *definitions;
	plugin_param *params;

	printf ("==================================================\n");
	printf ("name:\t%s\n", i_plugin_info->name);
	printf ("description:\t%s\n\n", i_plugin_info->description);
	printf ("argument\ttype\tdefinition\n");
	printf ("--------------------------------------------------\n");
	gint i;
	for (i = 0; i < i_plugin_info->param_number; i++)
		printf ("%d\t%s\t%s\n", i,
			plugin_type2string (i_plugin_info->types[i]),
			i_plugin_info->definitions[i]);
	printf ("==================================================\n");
}

/** create new plugin_return object
 *
 * @param i_param_number number of parameters
 *
 * free object with plugin_return _free
 *
 * \ingroup plugin
 */
plugin_return *
plugin_return_new (int i_param_number)
{
	plugin_return *o_plugin_return = g_new (plugin_return, 1);
	o_plugin_return->param_number = i_param_number;
	o_plugin_return->types = g_new (plugin_param_type, i_param_number);
	o_plugin_return->params = g_new (plugin_param, i_param_number);
	return o_plugin_return;
}

/** free plugin_return object
 *
 * @param i_plugin_return plugin_return object
 *
 * \ingroup plugin
 */
void
plugin_return_free (plugin_return *i_plugin_return)
{
	g_free (i_plugin_return->types);
	g_free (i_plugin_return->params);
	g_free (i_plugin_return);
}

