#include "didgmo.h"

int
main (int argc, char **argv)
{
	if (argc < 2)
	{
		printf ("------------------------------\n");
		printf ("%s %s\n", PACKAGE, VERSION);
		printf ("------------------------------\n");
		printf ("usage: %s [module] {arguments}\n\n", argv[0]);
		return -1;
	}

	plugin *l_plugin = plugin_new (argv[1]);
	{
		// get plugin_info
		plugin_info *info = plugin_get_info (l_plugin);
		//printf ("plugin: %s (%s)\n",
		//	info->name,
		//	info->description);

		plugin_init (l_plugin, argc-2, &(argv[2]));
		plugin_return *res = plugin_run (l_plugin, NULL);
		int status = res->params[0].d_int;
		if (status)
			printf ("module execution not successful\n");
		plugin_return_free (res);
		plugin_deinit (l_plugin);
	}
	plugin_free (l_plugin);

	return 0;
}

