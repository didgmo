/** \defgroup fft fft
 *
 * Routines for interaction with fft objects.
 *
 * create initial fft object for a range of 800Hz
 * @verbatim
const int fft_size = 800;
fft *fft_obj = fft_new (fft_size);@endverbatim
 * save your fft object
 * @verbatim
const char *fft_path = "my_obj.fft";
fft_save (fft_obj, fft_path)@endverbatim
 * free your fft object, when you don't need it anymore
 * @verbatim
fft_free (fft_obj);@endverbatim
 * load an fft object from a file
 * @verbatim
fft_obj = fft_load (path);@endverbatim
 * convert the amplitudes to dB
 * @verbatim
fft *fft_obj_dB = fft_dB (fft_obj);
fft_free (fft_obj_dB);@endverbatim
 * convert fft object to wave object via inverse fast fourier transformation
 * @verbatim
wave *wave_obj = fft_wave (fft_obj);
wave_free (wave_obj);@endverbatim
 * extract peaks of impedance (0), base (1) and overblow (2) sound spectra from fft object
 * @verbatim
peak *peak_obj_imp = fft_peak (fft_obj, 0);
peak *peak_obj_bas = fft_peak (fft_obj, 1);
peak *peak_obj_ovr = fft_peak (fft_obj, 2);
peak_free (peak_obj_imp);
peak_free (peak_obj_bas);
peak_free (peak_obj_ovr);@endverbatim
 * free your fft object, when you don't need it anymore
 * @verbatim
fft_free (fft_obj);@endverbatim
 *
 * \ingroup didgmo
 */
#include <fftw3.h>
#include <math.h>

#include "didgmo.h"

/** creates fft object with frequency range from 0 to i_size.
 *
 * @param i_size frequency range.
 *
 * @return fft object (free it with fft_free)
 *
 * \ingroup fft
 */
fft *
fft_new (double i_max, double i_step)
{
	fft *o_fft = g_new (fft, 1);
	o_fft->max = i_max;
	o_fft->step = i_step;
	o_fft->size = i_max / i_step;
	o_fft->imp = g_new (double, o_fft->size);
	o_fft->ovr = g_new (double, o_fft->size);
	o_fft->snd = g_new (double, o_fft->size);
	return o_fft;
}

/** frees fft object.
 *
 * @param i_fft fft object
 *
 * \ingroup fft
 */
void
fft_free (fft * i_fft)
{
	g_free (i_fft->snd);
	g_free (i_fft->ovr);
	g_free (i_fft->imp);
	g_free (i_fft);
}

/** creates wave object with reverse fourier transformation.
 *
 * @param i_fft fft object
 *
 * @return wave object (free it with wave_free)
 *
 * \ingroup fft
 */
wave *
fft_wave (fft *i_fft, int i_rate)
{
	const int sr = 44100;
	const int n = i_fft->size;
  int i;

  static double *rdata = NULL,*idata = NULL;
  static fftw_plan rplan;

  rdata = (double *) fftw_malloc (n * sizeof (double));
	idata = (double *) fftw_malloc (n * sizeof (double));
  rplan = fftw_plan_r2r_1d(n, rdata, idata, FFTW_DHT, FFTW_ESTIMATE);
	for (i = 0; i < n; i++)
		idata[i] = 0;
	for (i = 0; i < n; i++)
		rdata[i] = i_fft->snd[i];
	fftw_execute(rplan);
  
	wave *o_wave = wave_new (n);
	for (i = 0; i < n; i++)
		o_wave->array[i] = idata[i] / (double) n;
        
	fftw_destroy_plan(rplan);
	fftw_free(rdata);
	fftw_free(idata);

	return o_wave;
}

/** creates fft object with sound spectra of ground tone
 * and first overblow expressed in dB.
 *
 * @param i_fft fft object
 *
 * @return fft object (free it with fft_free);
 *
 * \ingroup fft
 */
fft *
fft_dB (fft *i_fft)
{
	fft *o_fft = fft_new (i_fft->max, i_fft->step);
	int i;
	for (i = 0; i < i_fft->size; i ++)
	{
		o_fft->imp[i] = i_fft->imp[i];
		o_fft->ovr[i] = 20 * (log10 (i_fft->ovr[i] * 2e-5));
		o_fft->snd[i] = 20 * (log10 (i_fft->snd[i] * 2e-5));
	}
	return o_fft;
}

/** save fft object to file.
 *
 * @param i_fft fft object
 * @param i_path file path to save to
 *
 * \ingroup fft
 */
void
fft_save (fft *i_fft, char *i_path)
{
	FILE *file = fopen (i_path, "w");
	int i;
	for (i = 1; i < i_fft->size; i++)
		fprintf (file, "%f %f %f %f\n",
			i_fft->step * i,
			i_fft->imp[i],
			i_fft->snd[i],
			i_fft->ovr[i]);
	fclose (file);
}

/** load fft object from file.
 *
 * @param i_path path to load from
 * @param i_size frequency range to load
 *
 * @return fft object (free it with fft_free)
 *
 * \ingroup fft
 */
fft *
fft_load (char *i_path)
{
	// TODO
}

/** extract peak information from fft object.
 *
 * @param i_fft fft object
 * @param i_id extract peaks from impedance spectrum (0), sound spectrum from ground tone (1) or sound spectrum from first overblow (2)
 *
 * @return peak object (free it with peak_free)
 *
 * \ingroup fft
 */
peak *
fft_peak (fft *i_fft, int i_id)
{
	double *ptr = NULL;
	switch (i_id)
	{
		case 0:
			ptr = i_fft->imp;
			break;
		case 1:
			ptr = i_fft->snd;
			break;
		case 2:
			ptr = i_fft->ovr;
			break;
	}

	// search for peaks and valleys
	int peaks [32];
	int vally [32];
  int i, up = 0, npeaks = 0, nvally = 0;
  for (i = 2; i < i_fft->size; i++) {
    if (ptr[i] > ptr[i-1])
		{
			if (npeaks && !up)
			{
				vally [nvally] = i - 1;
				nvally++;
			}
			up = 1;
		}
    else
		{
    	if (up)
			{
      	peaks[npeaks] = i - 1;
				npeaks++;
    	}
			up = 0;
		}
  }

	peak *o_peak = peak_new (nvally, npeaks);
	for (i = 0; i < nvally; i++)
	{
		//printf ("minimum found at: %d\n", vally[i]);
		o_peak->min_freq[i] = vally[i] * i_fft->step;
		o_peak->min_amp[i] = ptr[vally[i]];
	}
	for (i = 0; i < npeaks; i++)
	{
		//printf ("maximum found at: %d\n", peaks[i]);
		o_peak->max_freq[i] = peaks[i] * i_fft->step;
		o_peak->max_amp[i] = ptr[peaks[i]];
	}

	return o_peak;
}

