#include "didgmo.h"

int
main (int argc, char **argv)
{
	char path[256];

	sprintf (path, "%s.geo", argv[1]);
	geo *geo1 = geo_load (path);

	sprintf (path, "%s.eog", argv[1]);
	geo_eog (geo1, path);

	fft *fft1 = geo_fft (geo1, 1000.0, 1.0, 0);
	sprintf (path, "%s.fft", argv[1]);
	fft_save (fft1, path);

	peak *peak1 = fft_peak (fft1, 1); // peaks of ground tone spectrum
	sprintf (path, "%s.peak1", argv[1]);
	peak_save (peak1, path);

	peak *peak2 = fft_peak (fft1, 2); // peaks of ground tone spectrum
	sprintf (path, "%s.peak2", argv[1]);
	peak_save (peak2, path);

	peak_free (peak2);
	peak_free (peak1);
	fft_free (fft1);
	geo_free (geo1);

	return 1;
}

