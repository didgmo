/** \defgroup gnuplot gnuplot
 *
 * @param name name of modelling project
 *
 * @return NULL
 *
 * \ingroup modules
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h> // mkfifo

#include "didgmo.h"

static char *i_name;
static char *i_template;
static char CMD[1024];
static FILE *fifo;

static plugin_param_type types [] = {
	T_STRING,
	T_STRING
};

static plugin_param_def defs [] = {
	"modelling project name",
	"gnuplot template name"
};

static plugin_param params [2];

static plugin_info PLUGIN_INFO = {
	"gnuplot",
	"draws geo, imp and fft spectra of given project name according to a template",
	2,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	i_name = PLUGIN_INFO.params[0].d_string;
	i_template = PLUGIN_INFO.params[1].d_string;

	sprintf (CMD, "%s.fifo", i_name);
	mkfifo (CMD, S_IRUSR | S_IWUSR);

	sprintf (CMD, "gnuplot -persist < %s.fifo >& /dev/null &", i_name),
	system (CMD);

	sprintf (CMD, "%s.fifo", i_name);
	fifo = fopen (CMD, "w");
}

void
deinit ()
{
	fprintf (fifo, "quit ()\n", i_name),
	fflush (fifo);
	fclose (fifo);

	sprintf (CMD, "%s.fifo", i_name);
	remove (CMD);
}

plugin_return *
run (void *i_data)
{
	// template file resides in PKGSYSCONFDIR
	fprintf (fifo, "call '%s/%s.gpi' '%s'\n", PKGSYSCONFDIR, i_template, i_name),
	// TODO change template name and add fallback
	fflush (fifo);

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (1);
	ret->types[0] = T_STATUS;
	ret->params[0].d_int = 0;

	return ret;
}

