/** \defgroup scan_conical scan_conical
 *
 * @param name name of modelling project
 * @param length length of didg
 * @param n_seg number of segments
 * @param d0 mouth diameter
 * @param d1 bell diameter
 * @param db diameter band
 * @param di diameter increment
 * @param rank_mod ranking module
 *
 * @return NULL
 *
 * \ingroup modules
 */

#include <stdio.h>
#include <stdlib.h>

#include "didgmo.h"

static char *name;

const double lm = 0.01; // length mouth
const double dm = 0.03; // diameter mouth
static double l; // length
static int ns; // number of segments
static double ls; // length of one segment
static double d0; // mouth diameter
static double d1; // tail diameter
static double dd; // diameter difference
static double db; // diameter band
static double di; // diameter increment

static double *mean;
static double *min;
static double *max;

static plugin *rank_module;

geo *best_geo = NULL;
double best_rank = 0;

static plugin_param_type types [] = {
	T_STRING,
	T_FLOAT,
	T_INT,
	T_FLOAT,
	T_FLOAT,
	T_FLOAT,
	T_FLOAT,
	T_PLUGIN
};

static plugin_param_def defs [] = {
	"modelling project name",
	"didgeridoo length",
	"segment number",
	"diameter mouth end",
	"diameter bell end",
	"diameter dynamic band",
	"diameter dynamic step size",
	"rank module"
};

static plugin_param params [8];

static plugin_info PLUGIN_INFO = {
	"scan_conical",
	"scan algorithm that assumes fixed segment length, but varies diameters in a given band with given stepsize",
	8,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
iterate (int seg)
{
	if (seg < ns)
	{
		double id;
		for (id = min[seg]; id < max[seg]; id += di)
		{
			//printf ("%d %f\n", seg, id);
			mean[seg] = id;
			iterate (seg + 1);
		}
	}
	else
	{
		geo* geom = geo_new ();
		int s;
		for (s = 2; s < ns; s++)
			geo_push_tail (geom, seg_new (ls, mean[s-1], mean[s]));
		geo_push_tail (geom, seg_new (ls, mean[ns-1], d1));
		geo_push_head (geom, seg_new (ls, d0, mean[1]));
		geo_push_head (geom, seg_new (lm, dm, d0));

		fft *ffts = geo_fft (geom, 400, 1.0, 0);
		peak *peaks = fft_peak (ffts, 1); // 1 = ground tone
		//printf ("%i %f\n", peaks[0].freq, (double) peaks[0].amp);
		plugin_return *res = plugin_run (rank_module, (void *) peaks);
		int status = res->params[0].d_int;
		double rank = (double) res->params[1].d_float;
		peak_free (peaks);
		fft_free (ffts);

		if (!status && (rank > best_rank))
		{
			best_rank = rank;
			if (best_geo)
				geo_free (best_geo);
			best_geo = geom;
		}
		else
		{
			geo_free (geom);		
		}
		plugin_return_free (res);
	}
}

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	name = PLUGIN_INFO.params[0].d_string;
	l = PLUGIN_INFO.params[1].d_float;
	ns = PLUGIN_INFO.params[2].d_int;
	ls = l / (double) ns; // length of one segment
	d0 = PLUGIN_INFO.params[3].d_float;
	d1 = PLUGIN_INFO.params[4].d_float;
	dd = d1 - d0; // diameter difference
	db = PLUGIN_INFO.params[5].d_float;
	di = PLUGIN_INFO.params[6].d_float;

	rank_module = plugin_new (PLUGIN_INFO.params[7].d_plugin);
	plugin_init (rank_module, argc - 8, &(argv[8]));
}

void
deinit ()
{
	plugin_deinit (rank_module);
	plugin_free (rank_module);
}

plugin_return *
run (void *i_data)
{
	mean = g_new (double, ns+1);
	min = g_new (double, ns+1);
	max = g_new (double, ns+1);
	int s;
	for (s = 0; s < ns+1; s++)
	{
		mean[s] = d0 + s*dd/ns;
		min[s] = mean[s] - db/2;
		max[s] = mean[s] + db/2;
		if (min[s] < d0) min[s] = d0;
		if (max[s] > d1) max[s] = d1;
	}

	iterate (1);

	if (best_geo)
	{
		char path[256];
		printf ("rank: %f\n", (double) best_rank);
		sprintf (path, "%s.eog", name);
			geo_eog (best_geo, path);
		sprintf (path, "%s.geo", name);
			geo_save (best_geo, path);
		fft *best_fft = geo_fft (best_geo, 800, 1.0, 0);
		sprintf (path, "%s.fft", name);
			fft_save (best_fft, path);
		peak *best_peak = fft_peak (best_fft, 1);
		sprintf (path, "%s.peak", name);
			peak_save (best_peak, path);

		peak_free (best_peak);
		geo_free (best_geo);
		fft_free (best_fft);
	}
	else
		printf ("no geometry meets your requirements\n");

	g_free (mean);
	g_free (min);
	g_free (max);

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (1);
	ret->types[0] = T_STATUS;
	ret->params[0].d_int = 0;

	return ret;
}

