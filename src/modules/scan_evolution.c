/* TODO
 * work on initialization
 * check for overall diameter band dependent on position
 * load templeate geometry file
 */

/** \defgroup scan_evolution scan_evolution
 *
 * @param name name of modelling project
 * @param l0 minimal length of didg
 * @param l1 maximal length of didg
 * @param ns number of segments
 * @param np size of pool
 * @param ni number of iterations
 * @param d0 mouth diameter
 * @param d1 bell diameter
 * @param mf mutation frequency
 * @param s0 minimal segment length
 * @param si segment increment
 * @param db diameter band
 * @param di diameter increment
 * @param rank_mod ranking module
 *
 * @return NULL
 *
 * \ingroup modules
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "didgmo.h"

typedef struct _container container;
struct _container {
	geo *geo;
	double rank;
};

static char *name;

static double l0; // minimal length
static double l1; // maximal length
static double la; // average length
static int ns; // number of segments
static int np; // number of pool
static int ni; // number of iterations
static double d0; // mouth diameter
static double d1; // tail diameter
static double dd; // diameter difference
static double mf; // mutation frequency
static double s0; // minimal segment length
static double si; // segment increment
static double db; // diameter band
static double di; // diameter increment

static plugin *rank_module;

GQueue *evpool;

static plugin_param_type types [] = {
	T_STRING,
	T_FLOAT,
	T_FLOAT,
	T_INT,
	T_INT,
	T_INT,
	T_FLOAT,
	T_FLOAT,
	T_FLOAT,
	T_FLOAT,
	T_FLOAT,
	T_FLOAT,
	T_FLOAT,
	T_PLUGIN
};

static plugin_param_def defs [] = {
	"modelling project name",
	"minimal length",
	"maximal length",
	"number of segments",
	"size of pool",
	"number of iterations",
	"diameter mouth end",
	"diameter bell end",
	"mutation frequency",
	"minimal dynamic segment length",
	"dynamic segment step size",
	"dynamic diameter band",
	"dynamic diameter step size",
	"rank module"
};

static plugin_param params [14];

static plugin_info PLUGIN_INFO = {
	"scan_evolution",
	"scan algorithm, which does intelligent evolution",
	14,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	name = PLUGIN_INFO.params[0].d_string;
	l0 = PLUGIN_INFO.params[1].d_float;
	l1 = PLUGIN_INFO.params[2].d_float;
	la = (l0 + l1) / 2.0;
	ns = PLUGIN_INFO.params[3].d_int;
	np = PLUGIN_INFO.params[4].d_int;
	ni = PLUGIN_INFO.params[5].d_int;
	d0 = PLUGIN_INFO.params[6].d_float;
	d1 = PLUGIN_INFO.params[7].d_float;
	dd = d1 - d0;
	mf = PLUGIN_INFO.params[8].d_float;
	s0 = PLUGIN_INFO.params[9].d_float;
	si = PLUGIN_INFO.params[10].d_float;
	db = PLUGIN_INFO.params[11].d_float;
	di = PLUGIN_INFO.params[12].d_float;

	rank_module = plugin_new (PLUGIN_INFO.params[13].d_plugin);
	plugin_init (rank_module, argc - 14, &(argv[14]));

	evpool = g_queue_new ();

	srand (time (NULL));
}

void
deinit ()
{
	plugin_deinit (rank_module);
	plugin_free (rank_module);

	g_queue_free (evpool);
}

float frand ()
{
	return (float) rand () / (float) RAND_MAX;
}

void
init_pool (GQueue *evpool)
{
	gint p; // loop over pool
	for (p = 0; p < np; p++)
	{
		container *cont = g_new (container, 1);
		cont->geo = geo_new ();
		gint s; // loop over segments
		for (s = 0; s < ns; s++)
		{
			seg *segm = seg_new (
				la / ns,
				d0 + dd * s / ns,
				d0 + dd * (s + 1) / ns);
			geo_push_tail (cont->geo, segm);
		}
		char path [256];
		sprintf (path, "tmp_%d.geo", p);
		g_queue_push_tail (evpool, cont);
	}
}

void
run_pool_func (container *i_cont, void *udata)
{
		fft *ffts = geo_fft (i_cont->geo, 600, 1.0, 0);
		peak *peaks = fft_peak (ffts, 1); // 1 = ground tone
		plugin_return *res = plugin_run (rank_module, (void *) peaks);
		int status = res->params[0].d_int;
		if (!status)
			i_cont->rank = (double) res->params[1].d_float;
		else
			i_cont->rank = 0.0;
		peak_free (peaks);
		fft_free (ffts);
		plugin_return_free (res);
}

gint
run_pool_sort (container *i_cont1, container *i_cont2, void *udata)
{
	return (i_cont1->rank) < (i_cont2->rank);	
}

gint run_pool_mix (container *i_cont1, container *i_cont2, void *udata)
{
	return frand () < 0.5;
}

void run_pool (GQueue *evpool)
{
	// calculate fft spectra for individui
	g_queue_foreach (evpool, run_pool_func, NULL);
	// sort individui according to their ranking
	g_queue_sort (evpool, run_pool_sort, NULL);
}

void
recombinate_pool (GQueue *evpool)
{
	// delete third third
	gint i;
	for (i = 0; i < np / 3; i++)
	{
		container *i_cont = g_queue_pop_tail (evpool);
		geo_free (i_cont->geo);
		g_free (i_cont);
	}

	// mix pool
	for (i = 0; i < 10; i++)
		g_queue_sort (evpool, run_pool_mix, NULL);

	// add recombinations of first and second half
	for (i = 0; i < np / 3; i++)
	{
		container *cont_male = g_queue_peek_nth (evpool, i);
		container *cont_female = g_queue_peek_nth (evpool, i + np / 3);
		container *o_cont = g_new (container, 1);
		o_cont->geo = geo_new ();
		gint s;
		for (s = 0; s < ns; s++)
		{
			float r = frand ();
			if (r < 0.5) // take segment from mail
				geo_push_tail (o_cont->geo,
					seg_copy ( geo_get_nth (cont_male->geo, s)));
			else // take segment from female
				geo_push_tail (o_cont->geo,
					seg_copy ( geo_get_nth (cont_female->geo, s)));
			// adjust link diameter
			if (s > 0) // only from second segment on
			{
				seg *seg1 = geo_get_nth (o_cont->geo, s - 1);
				seg *seg2 = geo_get_nth (o_cont->geo, s);
				double d3 = (seg1->d1 + seg2->d0) / 2.0;
				seg1->d1 = d3;
				seg2->d0 = d3;
				seg_update (seg1);
				seg_update (seg2);
			}
		}
		g_queue_push_tail (evpool, o_cont);
	}
}

void
mutate_pool_len_func (seg *i_seg, void *udata)
{
	i_seg->L -= *((double *) udata);
	if (i_seg->L < s0)
		i_seg->L = s0;
	seg_update (i_seg);
}

void
mutate_pool_func (container *i_cont, void *udata)
{
	gint s;
	for (s = 0; s < ns; s++)
	{
		seg *i_seg = geo_get_nth (i_cont->geo, s);
		float r = frand ();
		if (r < mf)
		{
			i_seg->L += si * 2.0 * (frand () - 0.5);
			if (i_seg->L < s0)
				i_seg->L = s0;
			if (s > 0) {
				i_seg->d0 += di * 2.0 * (frand () - 0.5);
				if (i_seg->d0 < d0)
					i_seg->d0 = d0;
				if (i_seg->d0 > d1)
					i_seg->d0 = d1;

				seg *i_seg0 = geo_get_nth (i_cont->geo, s - 1);
				i_seg0->d1 = i_seg->d0;
				seg_update (i_seg0);
			}
			seg_update (i_seg);
		}
	}
	// check overall length of geometry and correct if necessary
	double len = geo_length_get (i_cont->geo);
	while ((len < l0) || (len > l1))
	{
		double diff = (len - la) / (double) ns;
		g_queue_foreach (i_cont->geo, mutate_pool_len_func, &diff);
		len = geo_length_get (i_cont->geo);
	}
}

void
mutate_pool (GQueue *evpool)
{
	g_queue_foreach (evpool, mutate_pool_func, NULL);
}

plugin_return *
run (void *i_data)
{
	gint i = 0;

	init_pool (evpool);
	for (i = 0; i < 20; i++)
		mutate_pool (evpool);

	i = 0;
	while (i++ < ni)
	{
		run_pool (evpool);
		printf ("generation: %d (%f)\n",
			i, (double) ((container *) g_queue_peek_head (evpool))->rank);
		recombinate_pool (evpool);
		mutate_pool (evpool);
	}

	// sort individui according to their ranking
	g_queue_sort (evpool, run_pool_sort, NULL);

	container *cont_best = g_queue_peek_head (evpool);
	geo *best_geo = cont_best->geo;
	double best_rank = cont_best->rank;

	char path[256];
	printf ("rank: %f\n", (double) best_rank);
	sprintf (path, "%s.eog", name);
		geo_eog (best_geo, path);
	sprintf (path, "%s.geo", name);
		geo_save (best_geo, path);
	fft *best_fft = geo_fft (best_geo, 800, 1.0, 0);
	sprintf (path, "%s.fft", name);
		fft_save (best_fft, path);
	peak *best_peak = fft_peak (best_fft, 1);
	sprintf (path, "%s.peak", name);
		peak_save (best_peak, path);

	peak_free (best_peak);
	geo_free (best_geo);
	fft_free (best_fft);

	// TODO free all

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (1);
	ret->types[0] = T_STATUS;
	ret->params[0].d_int = 0;

	return ret;
}

