/** \defgroup geo2circ geo2circ
 *
 * @param name name of modelling project
 *
 * @return NULL
 *
 * \ingroup modules
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "didgmo.h"

static char *i_name;

static plugin_param_type types [] = {
	T_STRING
};

static plugin_param_def defs [] = {
	"modelling project name",
};

static plugin_param params [1];

static plugin_info PLUGIN_INFO = {
	"geo2circ",
	"converts a *.geo to an *.circ (circumpherence) file",
	1,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	i_name = PLUGIN_INFO.params[0].d_string;
}

void
deinit ()
{}

double tmp_len;

void
_circ (gpointer data, gpointer user_data)
{
	seg *i_seg = (seg *) data;
	FILE *file = (FILE *) user_data;
	tmp_len += i_seg->L;
	fprintf (file, "%f %f\n", tmp_len, (double) i_seg->d1 * 2 * M_PI);
}

plugin_return *
run (void *i_data)
{
	char path[256];
	sprintf (path, "%s.geo", i_name);
	geo *i_geo = geo_load (path);

	sprintf (path, "%s.circ", i_name);
	FILE *file = fopen (path, "w");
	tmp_len = 0.0;
	seg *head = (seg *) g_queue_peek_head (i_geo);
	fprintf (file, "%f %f\n", 0.0, (double) head->d0 * 2 * M_PI);
	g_queue_foreach (i_geo, _circ, file);
	fclose (file);

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (1);
	ret->types[0] = T_STATUS;
	ret->params[0].d_int = 0;

	return ret;
}

