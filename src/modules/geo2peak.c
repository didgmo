/** \defgroup geo2peak geo2peak
 *
 * @param name name of modelling project
 * @param freq_max maximal frequency [Hz] to simulate spectrum up to
 *
 * @return NULL
 *
 * \ingroup modules
 */

#include <stdio.h>
#include <stdlib.h>

#include "didgmo.h"

typedef struct _composite composite;

struct _composite {
	geo *s_geo;
	peak *s_peak;
};

static plugin *l_plugin = NULL;

static char *i_name;
static int i_max;

static plugin_param_type types [] = {
	T_STRING,
	T_INT
};

static plugin_param_def defs [] = {
	"modelling project name",
	"maximal frequency"
};

static plugin_param params [2];

static plugin_info PLUGIN_INFO = {
	"geo2peak",
	"simulate fft spectra based on a given geometry and return peaks",
	2,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	i_name = PLUGIN_INFO.params[0].d_string;
	i_max = PLUGIN_INFO.params[1].d_int;

	plugin *l_plugin = plugin_new (i_name);
	plugin_init (l_plugin, argc-2, &(argv[2]));
}

void
deinit ()
{
	plugin_deinit (l_plugin);
}

plugin_return *
run (void *i_data)
{
	char path[256];
	sprintf (path, "%s.geo", i_name);
	geo *i_geo = geo_load (path);

	sprintf (path, "%s.eog", i_name);
	geo_eog (i_geo, path);

	fft *o_fft = geo_fft (i_geo, i_max, 1.0, 0);
	sprintf (path, "%s.fft", i_name);
	fft_save (o_fft, path);

	peak *o_peak = fft_peak (o_fft, 0);
	composite * l_composite = g_new (composite, 1);
	l_composite->s_geo = i_geo;
	l_composite->s_peak = o_peak;
	plugin_run (l_plugin, l_composite);
	f_free (l_composite);

	peak_free (o_peak);
	fft_free (o_fft);
	geo_free (i_geo);

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (1);
	ret->types[0] = T_STATUS;
	ret->params[0].d_int = 0;

	return ret;
}

