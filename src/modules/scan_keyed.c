/** \defgroup scan_keyed scan_keyed
 *
 * @param name name of modelling project
 * @param diam diameter of tube
 * @param base frequency of base tone
 * @param num number of keys over one octave
 *
 * @return NULL
 *
 * \ingroup modules
 */

#include <stdio.h>
#include <stdlib.h>

#include "didgmo.h"

static char *i_name;
static double i_diam;
static double i_base;
static int i_num;

static plugin_param_type types [] = {
   T_STRING,
   T_FLOAT,
   T_FLOAT,
   T_INT
};

static plugin_param_def defs [] = {
   "modelling project name",
   "diameter of tube",
   "frequency of base tone",
   "number of keys over one octave"
};

static plugin_param params [4];

static plugin_info PLUGIN_INFO = {
   "scan_keyed",
   "calculate right tone hole diameter and positions for given key number over one octave",
   4,
   types,
   defs,
   params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
   PLUGIN_PARSE

   i_name = PLUGIN_INFO.params[0].d_string;
   i_diam = PLUGIN_INFO.params[1].d_float;
   i_base = PLUGIN_INFO.params[2].d_float;
   i_num  = PLUGIN_INFO.params[3].d_int;
}

void
deinit ()
{}

plugin_return *
run (void *i_data)
{
   char path[256];
   sprintf (path, "%s.geo", i_name);

   geo *o_geo = geo_new ();
   double l =  350 / 4.0 / i_base - 0.3 * i_diam;
   geo_push_head (o_geo, seg_new (l, i_diam, i_diam));
   geo_save (o_geo, path);
   geo_free (o_geo);

   sprintf (path, "%s.key", i_name);
   // TODO code me

   /*
   * return object
   */
   plugin_return *ret = plugin_return_new (1);
   ret->types[0] = T_STATUS;
   ret->params[0].d_int = 0;

   return ret;
}

