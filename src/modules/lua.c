/* TODO
 * implementation!!
 */

/** \defgroup lua lua
 *
 * @param path path to lua script file
 * @param num_params number of script parameters
 *
 * \ingroup modules
 */

#include <stdio.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include "didgmo.h"

extern int lua_open_didgmo (lua_State *L);

static gchar *path;
static lua_State *L;

static plugin_param_type types [] = {
	T_STRING
};

static plugin_param_def defs [] = {
	"path to lua script file",
};

static plugin_param params [1];

static plugin_info PLUGIN_INFO = {
	"lua",
	"develop plugins in the lua scripting language",
	1,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	path = PLUGIN_INFO.params[0].d_string;

	L = luaL_newstate ();
	luaL_openlibs (L);
	lua_open_didgmo (L);

	// load and process lua script
	if (luaL_dofile (L, path))
		printf ("lua parse error: %s\n", lua_tostring (L, -1));

	// read plugin info table
	lua_getglobal (L, "PLUGIN_INFO");
	lua_getfield (L, -1, "name");
	lua_getfield (L, -2, "description");
	lua_getfield (L, -3, "num_params");
	const gint sub_num_params = luaL_checkint (L, -1);
	lua_getfield (L, -4, "types");
	lua_getfield (L, -5, "defs");

	gint i;
	plugin_param_type sub_types [sub_num_params];
	plugin_param_def sub_defs [sub_num_params];
	plugin_param sub_params [sub_num_params];
	for (i = 0; i < sub_num_params; i++)
	{
		lua_rawgeti (L, -2, i + 1); // types
		sub_types[i] = luaL_checkint (L, -1);
		lua_pop (L, 1);

		lua_rawgeti (L, -1, i + 1); // defs
		sub_defs[i] = luaL_checkstring (L, -1);
		lua_pop (L, 1);
	}

	plugin_info SUB_PLUGIN_INFO = {
		luaL_checkstring (L, -5),
		luaL_checkstring (L, -4),
		sub_num_params,
		sub_types,
		sub_defs,
		sub_params
	};
	lua_pop (L, 6);

	plugin_parse (&SUB_PLUGIN_INFO, argc - 1, &(argv[1]));

	// run plugins init function
	lua_getglobal (L, "init");
	if (lua_isnil (L, -1))
	{
		printf ("lua no init function defined\n");
		lua_pop (L, 1);
	}
	else
	{
		int i;
		for (i = 0; i < sub_num_params; i++)
			switch (sub_types[i]) {
				case T_STRING:
					lua_pushstring (L, sub_params[i].d_string);
					break;
				case T_INT:
					lua_pushnumber (L, sub_params[i].d_int);
					break;
				case T_FLOAT:
					lua_pushnumber (L, sub_params[i].d_float);
					break;
				case T_PLUGIN:
					lua_pushstring (L, sub_params[i].d_plugin);
					break;
				case T_STATUS:
					lua_pushnumber (L, sub_params[i].d_int);
					break;
			}
		if (lua_pcall (L, sub_num_params, 0, 0))
			printf ("lua init error: %s\n", lua_tostring (L, -1));
	}
}

void
deinit ()
{
	lua_getglobal (L, "deinit");
	if (lua_isnil (L, -1))
	{
		printf ("lua no deinit function defined\n");
		lua_pop (L, 1);
	}
	else
	{
		if (lua_pcall (L, 0, 0, 0))	
			printf ("lua deinit error: %s\n", lua_tostring (L, -1));
	}

	lua_close (L);
}

plugin_return *
run (void *data)
{
	lua_getglobal (L, "run");
	if (lua_isnil (L, -1))
	{
		printf ("lua no run function defined\n");
		lua_pop (L, 1);
	}
	else
	{
		lua_pushnil (L);
		if (lua_pcall (L, 1, 1, 0))	
			printf ("lua run error: %s\n", lua_tostring (L, -1));
	}

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (1);
	ret->types[0] = T_STATUS;
	ret->params[0].d_int = (gint) lua_tonumber (L, -1);

	return ret;
}

