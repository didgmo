/** \defgroup wave2fft wave2fft
 *
 * @param name name of sound file
 * @param freq maximal frequency [Hz] to output
 * @param dur duration to scan through [s]
 *
 * @return NULL
 *
 * \ingroup modules
 */

#include <stdio.h>
#include <stdlib.h>
#include <gmodule.h>

#include "didgmo.h"

static char *i_name;
static int i_freq;
static int i_dur;

static plugin_param_type types [] = {
	T_STRING,
	T_INT,
	T_INT
};

static plugin_param_def defs [] = {
	"name of sound file",
	"maximal frequency",
	"sound duration"
};

static plugin_param params [3];

static plugin_info PLUGIN_INFO = {
	"wave2fft",
	"calculates fourier transformation of given wave file",
	3,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	i_name = PLUGIN_INFO.params[0].d_string;
	i_freq = PLUGIN_INFO.params[1].d_int;
	i_dur = PLUGIN_INFO.params[2].d_int;
}

void
deinit ()
{}

plugin_return *
run (void *i_data)
{
	wave *i_wave = wave_load (i_name, i_dur);
	fft *o_fft = wave_fft (i_wave, i_freq);

	char path [256];
	sprintf (path, "%s.fft", i_name);
	fft_save (o_fft, path);

	fft_free (o_fft);
	wave_free (i_wave);

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (1);
	ret->types[0] = T_STATUS;
	ret->params[0].d_int = 0;

	return ret;
}

