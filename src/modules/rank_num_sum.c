/** \defgroup rank_num_sum rank_num_sum
 *
 * @param freq_low lower limit of frequencies
 * @param freq_high higher limit of frequencies
 *
 * @return the actual ranking
 *
 * \ingroup modules
 */

#include <stdio.h>
#include "didgmo.h"

static int freq_low_band;
static int freq_high_band;
static double diff_ground;

static plugin_param_type types [] = {
	T_INT,
	T_INT,
	T_FLOAT
};

static plugin_param_def defs [] = {
	"frequency low band",
	"frequency high band",
	"difference in dB from ground tone"
};

static plugin_param params [3];

static plugin_info PLUGIN_INFO = {
	"rank_num_sum",
	"rank peaks according to number of peaks",
	3,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	freq_low_band = PLUGIN_INFO.params[0].d_int;
	freq_high_band = PLUGIN_INFO.params[1].d_int;
	diff_ground = PLUGIN_INFO.params[2].d_float;
}

void
deinit ()
{

}

plugin_return *
run (void *p_peak)
{
	peak *i_peak = (peak *) p_peak;

	if ( (i_peak->max_freq[0] < freq_low_band)
		|| (i_peak->max_freq[0] > freq_high_band))
	{
		/*
		 * return object
		 */
		plugin_return *ret = plugin_return_new (1);
		ret->types[0] = T_STATUS;
		ret->params[0].d_int = 1;

		return ret;
	}

	double res;;
	res = 0.0;
	int p;
	for (p = 0; p < i_peak->max_size; p++)
	{
		gdouble dB0 = 20 * (log10 (i_peak->max_amp[0] * 2e-5));
		gdouble dBp = 20 * (log10 (i_peak->max_amp[p] * 2e-5));
		gdouble diff = dB0 - dBp;
		if (diff < diff_ground)
			res += 10 + dB0 / dBp;
	}

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (2);
	ret->types[0] = T_STATUS;
	ret->types[1] = T_FLOAT;
	ret->params[0].d_int = 0;
	ret->params[1].d_float = (double) res;

	return ret;
}

