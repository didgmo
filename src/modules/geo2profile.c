/** \defgroup geo2profile geo2profile
 *
 * @param name name of modelling project
 * @param step step size
 *
 * @return NULL
 *
 * \ingroup modules
 */

#include <stdio.h>
#include <stdlib.h>

#include "didgmo.h"

static char *i_name;
static double i_step;

static plugin_param_type types [] = {
   T_STRING,
   T_FLOAT
};

static plugin_param_def defs [] = {
   "modelling project name",
   "step size"
};

static plugin_param params [2];

static plugin_info PLUGIN_INFO = {
   "geo2profile",
   "simulate fft spectra based on a given geometry and return peaks",
   2,
   types,
   defs,
   params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
   PLUGIN_PARSE

   i_name = PLUGIN_INFO.params[0].d_string;
   i_step = PLUGIN_INFO.params[1].d_float;
}

void
deinit ()
{
}

double tmp_len;

void
_header (FILE *file, double d)
{
   fprintf (file, "     L      d");
   double t;
   for (t = i_step; t <= d/2.0; t += i_step)
     fprintf (file, " %4.4f", t);
   fprintf (file, "\n");
}

void
_profile (FILE *file, double l, double d)
{
   fprintf (file, "%4.4f %4.4f", l, d);
   double t;
   for (t = i_step; t <= d/2.0; t += i_step)
     fprintf (file, " %4.4f", sqrt (d*d/4.0 - t*t));
   fprintf (file, "\n");
}

void
_prof_cb (gpointer data, gpointer user_data)
{
   seg *i_seg = (seg *) data;
   FILE *file = (FILE *) user_data;
   tmp_len += i_seg->L;
   _profile (file, tmp_len, (double) i_seg->d1);
}

plugin_return *
run (void *i_data)
{
   char path[256];
   sprintf (path, "%s.geo", i_name);
   geo *i_geo = geo_load (path);

   sprintf (path, "%s.profile", i_name);
   FILE *file = fopen (path, "w");
   tmp_len = 0.0;
   seg *head = (seg *) g_queue_peek_head (i_geo);
   seg *tail = (seg *) g_queue_peek_tail (i_geo);
   _header (file, tail->d1);
   _profile (file, 0.0, (double) head->d0);
   g_queue_foreach (i_geo, _prof_cb, file);
   fclose (file);

   geo_free (i_geo);

   /*
   * return object
   */
   plugin_return *ret = plugin_return_new (1);
   ret->types[0] = T_STATUS;
   ret->params[0].d_int = 0;

   return ret;
}

