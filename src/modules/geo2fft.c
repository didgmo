/** \defgroup geo2fft geo2fft
 *
 * @param name name of modelling project
 * @param freq_max maximal frequency [Hz] to simulate spectrum up to
 * @param freq_step step frequency unit
 *
 * @return NULL
 *
 * \ingroup modules
 */

#include <stdio.h>
#include <stdlib.h>

#include "didgmo.h"

static char *i_name;
static double i_max;
static double i_step;

static plugin_param_type types [] = {
	T_STRING,
	T_FLOAT,
	T_FLOAT
};

static plugin_param_def defs [] = {
	"modelling project name",
	"maximal frequency",
	"step frequency"
};

static plugin_param params [3];

static plugin_info PLUGIN_INFO = {
	"geo2fft",
	"simulate fft spectra based on a given geometry",
	3,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	i_name = PLUGIN_INFO.params[0].d_string;
	i_max = PLUGIN_INFO.params[1].d_float;
	i_step = PLUGIN_INFO.params[2].d_float;
}

void
deinit ()
{}

plugin_return *
run (void *i_data)
{
	char path[256];
	sprintf (path, "%s.geo", i_name);
	geo *i_geo = geo_load (path);

	sprintf (path, "%s.eog", i_name);
	geo_eog (i_geo, path);

	fft *o_fft = geo_fft (i_geo, i_max, i_step, 0);
	sprintf (path, "%s.fft", i_name);
	fft_save (o_fft, path);

	peak *o_peak = fft_peak (o_fft, 1);
	sprintf (path, "%s.peak", i_name);
	peak_save (o_peak, path);

	sprintf (path, "%s.lab", i_name);
	peak_label (o_peak, path);

	peak_free (o_peak);
	fft_free (o_fft);
	geo_free (i_geo);

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (1);
	ret->types[0] = T_STATUS;
	ret->params[0].d_int = 0;

	return ret;
}

