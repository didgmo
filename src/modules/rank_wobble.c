/** \defgroup rank_num_sum rank_num_sum
 *
 * @param freq_low lower limit of frequencies
 * @param freq_high higher limit of frequencies
 *
 * @return the actual ranking
 *
 * \ingroup modules
 */

#include <stdio.h>
#include "didgmo.h"

static int freq_low_band;
static int freq_high_band;
static double diff_ground;
static double diff_wobble;

static plugin_param_type types [] = {
	T_INT,
	T_INT,
	T_FLOAT,
	T_FLOAT
};

static plugin_param_def defs [] = {
	"frequency low band",
	"frequency high band",
	"maximal difference between wobblink peak and ground peak",
	"maximal difference between wobbling peaks"
};

static plugin_param params [4];

static plugin_info PLUGIN_INFO = {
	"rank_wobble",
	"rank peaks wheteher they are wobble enabled",
	4,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	freq_low_band = PLUGIN_INFO.params[0].d_int;
	freq_high_band = PLUGIN_INFO.params[1].d_int;
	diff_ground = PLUGIN_INFO.params[2].d_float;
	diff_wobble = PLUGIN_INFO.params[3].d_float;
}

void
deinit ()
{

}

plugin_return *
run (void *p_peak)
{
	peak *i_peak = (peak *) p_peak;

	if ( (i_peak->max_freq[0] < freq_low_band)
		|| (i_peak->max_freq[0] > freq_high_band))
	{
		/*
		 * return object
		 */
		plugin_return *ret = plugin_return_new (1);
		ret->types[0] = T_STATUS;
		ret->params[0].d_int = 1;

		return ret;
	}

	double res;
	res = 0.0;
	
	// loop over peaks
	gint i;
	for (i = 2; i < i_peak->max_size - 1; i++)
	{
		// convert to dB
		gdouble dB0 = 20 * (log10 (i_peak->max_amp[0] * 2e-5));
		gdouble dBi = 20 * (log10 (i_peak->max_amp[i] * 2e-5));
		gdouble dBi1 = 20 * (log10 (i_peak->max_amp[i+1] * 2e-5));

		// express as relative
		gdouble rel1 = dBi / dB0;
		gdouble rel2 = dBi1 / dBi;

		// compare peaks
		gdouble diff1 = dB0 - dBi;
		gdouble diff2 = dBi - dBi1;
		
		if ( (diff1 < diff_ground) && (diff2 < diff_wobble) )
			res += rel1 * rel2;
	}

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (2);
	ret->types[0] = T_STATUS;
	ret->types[1] = T_FLOAT;
	ret->params[0].d_int = 0;
	ret->params[1].d_float = (double) res;

	return ret;
}

