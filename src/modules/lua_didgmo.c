#include <stdio.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include "didgmo.h"

static void
ld_set_enum (lua_State *L, gint index, gchar *id, gint val)
{
	lua_pushnumber (L, val);
	lua_setfield (L, index, id);
}

static struct luaL_Reg ld_geo [] = {
	{NULL, NULL} // sentinel
};

static struct luaL_Reg ld_fft [] = {
	{NULL, NULL} // sentinel
};

static struct luaL_Reg ld_wave [] = {
	{NULL, NULL} // sentinel
};

static struct luaL_Reg ld_plugin [] = {
	{NULL, NULL} // sentinel
};

static struct luaL_Reg ld_peak [] = {
	{NULL, NULL} // sentinel
};

int lua_open_didgmo (lua_State *L)
{
	// define enums
	ld_set_enum (L, LUA_GLOBALSINDEX, "T_STRING", T_STRING);
	ld_set_enum (L, LUA_GLOBALSINDEX, "T_INT", T_INT);
	ld_set_enum (L, LUA_GLOBALSINDEX, "T_FLOAT", T_FLOAT);
	ld_set_enum (L, LUA_GLOBALSINDEX, "T_PLUGIN", T_PLUGIN);
	ld_set_enum (L, LUA_GLOBALSINDEX, "T_STATUS", T_STATUS);

	// define structs
	luaL_register (L, "geo", ld_geo);
	luaL_register (L, "fft", ld_fft);
	luaL_register (L, "wave", ld_wave);
	luaL_register (L, "plugin", ld_plugin);
	luaL_register (L, "peak", ld_peak);

	lua_pop (L, 5);

	return 0;
}

