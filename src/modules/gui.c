/** \defgroup gui gui
 *
 * starts the graphical user interface
 *
 * \ingroup modules
 */

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

#include "didgmo.h"

static plugin_param_type types [] = {
	T_PLUGIN
};

static plugin_param_def defs [] = {
	"plugin to start in gui mode"
};

static plugin_param params [1];

static plugin_info PLUGIN_INFO = {
	"gui",
	"graphical user interface",
	1,
	types,
	defs,
	params
};

PLUGIN_REGISTER

static char *name;
static plugin *module;
static plugin_info *module_info;
static GtkWidget **entries;

void
button_clicked (GtkButton *button)
{
	char *label = gtk_button_get_label (button);
	if (!strcmp (label, "Close"))
		gtk_main_quit ();
	else if (!strcmp (label, "Ok"))
	{
		int argc = module_info->param_number;
		char *argv [argc];
		int i;
		for (i = 0; i < module_info->param_number; i++)
			switch (module_info->types[i])
			{
				case T_STRING:
				case T_INT:
				case T_FLOAT:
					argv[i] =  gtk_entry_get_text (GTK_ENTRY (entries[i]));
					break;
				case T_PLUGIN:
					// TODO
					break;
			}
		plugin_init (module, argc, argv);
		plugin_return *ret = plugin_run (module, NULL);
		plugin_return_free (ret);
	}
}

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	name = PLUGIN_INFO.params[0].d_plugin;
	module = plugin_new (name);
	//plugin_init (module, argc - 1, &(argv[1]));
	module_info = plugin_get_info (module);

	gtk_init (&argc, &argv);
}

void
deinit ()
{
	plugin_free (module);
}

plugin_return *
run (void *i_data)
{
	GtkWidget *window;
	GtkWidget *vbox;
	GtkWidget *frame;
	GtkWidget *descr;
	GtkWidget *hbox [module_info->param_number + 1];
	GtkWidget *label [module_info->param_number];
	GtkWidget *entry [module_info->param_number];
	GtkWidget *button [2];

	entries = entry;
    
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width (GTK_CONTAINER (window), 10);
	gtk_widget_show (window);

	vbox = gtk_vbox_new (1, 10);
	gtk_container_add (GTK_CONTAINER (window), vbox);
	gtk_widget_show (vbox);

	frame = gtk_frame_new (NULL);
	gtk_container_add (GTK_CONTAINER (vbox), frame);
	gtk_widget_show (frame);

	descr = gtk_label_new	(module_info->description);
	gtk_container_add (GTK_CONTAINER (frame), descr);
	gtk_widget_show (descr);

	int i;
	for (i = 0; i < module_info->param_number; i++)
	{
		hbox[i] = gtk_hbox_new (1, 10);
		gtk_container_add (GTK_CONTAINER (vbox), hbox[i]);
		gtk_widget_show (hbox[i]);

		label[i] = gtk_label_new (module_info->definitions[i]);
		gtk_box_pack_start (GTK_BOX (hbox[i]), label[i], 1, 1, 10);
		gtk_widget_show (label[i]);

		entry[i] = gtk_entry_new ();
		gtk_box_pack_start (GTK_BOX (hbox[i]), entry[i], 1, 1, 10);
		gtk_widget_show (entry[i]);
	}

	hbox[module_info->param_number] = gtk_hbox_new (1, 10);
	gtk_container_add (GTK_CONTAINER (vbox), hbox[module_info->param_number]);
	gtk_widget_show (hbox[module_info->param_number]);

	button[0] = gtk_button_new_with_label ("Ok");
	gtk_box_pack_start (GTK_BOX (hbox[module_info->param_number]), button[0], 1, 1, 10);
	gtk_widget_show (button[0]);

	button[1] = gtk_button_new_with_label ("Close");
	gtk_box_pack_start (GTK_BOX (hbox[module_info->param_number]), button[1], 1, 1, 10);
	gtk_widget_show (button[1]);
	(GTK_BUTTON_GET_CLASS (button[1]))->clicked = button_clicked;

	gtk_main ();	

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (1);
	ret->types[0] = T_STATUS;
	ret->params[0].d_int = 0;

	return ret;
}

