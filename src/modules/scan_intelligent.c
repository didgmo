/** \defgroup scan_intelligent scan_intelligent
 *
 * @param name name of modelling project
 * @param length length of didg
 * @param n_seg number of segments
 * @param d0 mouth diameter
 * @param d1 bell diameter
 * @param s0 minimal segment length
 * @param si segment increment
 * @param db diameter band
 * @param di diameter increment
 * @param rank_mod ranking module
 *
 * @return NULL
 *
 * \ingroup modules
 */

#include <stdio.h>
#include <stdlib.h>

#include "didgmo.h"

static char *name;

const double lm = 0.01; // length mouth
const double dm = 0.03; // diameter mouth
static double l; // length
static int ns; // number of segments
static double d0; // mouth diameter
static double d1; // tail diameter
static double s0; // minimal segment length
static double si; // segment increment
static double db; // diameter band
static double di; // diameter increment

static double *mean;

static plugin *rank_module;

GQueue *evpool;
geo *best_geo = NULL;
double best_rank = 0;

static plugin_param_type types [] = {
	T_STRING,
	T_FLOAT,
	T_INT,
	T_FLOAT,
	T_FLOAT,
	T_FLOAT,
	T_FLOAT,
	T_FLOAT,
	T_FLOAT,
	T_PLUGIN
};

static plugin_param_def defs [] = {
	"modelling project name",
	"length",
	"number of segments",
	"diameter mouth end",
	"diameter bell end",
	"minimal dynamic segment length",
	"dynamic segment step size",
	"dynamic diameter band",
	"dynamic diameter step size",
	"rank module"
};

static plugin_param params [10];

static plugin_info PLUGIN_INFO = {
	"scan_intelligent",
	"scan algorithm, which assumes fixed didg length, but varies segment length and diameter",
	10,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
iterate_seg (int nseg, double left)
{
	if (nseg < ns - 1)
	{
		double id;
		double right;

		right = l - left - (si * (ns - 1 - nseg));

		for (id = si; id <= right; id += si)
		{
			mean[nseg] = id;
			iterate_seg (nseg + 1, left + id);
		}
	}
	else
	{
		geo* geom = geo_new ();
		int s;
		for (s = 0; s < ns - 1; s++)
			geo_push_tail (geom, seg_new (mean[s], d0 + di*s, d0 + di*(s+1)));
		geo_push_tail (geom, seg_new (l - left, d1 - di, d1));
		geo_push_head (geom, seg_new (lm, dm, d0));

		fft *ffts = geo_fft (geom, 400, 1.0, 0);
		peak *peaks = fft_peak (ffts, 1); // 1 = ground tone
		plugin_return *res = plugin_run (rank_module, (void *) peaks);
		int status = res->params[0].d_int;
		double rank = (double) res->params[1].d_float;
		peak_free (peaks);
		fft_free (ffts);

		if (!status && (rank > best_rank))
		{
			best_rank = rank;

			// add to evpool
			g_queue_push_head (evpool, geom);
		}
		else
		{
			geo_free (geom);		
		}
		plugin_return_free (res);
	}
}

void iterate_diam (geo *geom, int *nseg)
{
	if (*nseg < ns)
	{
		double d;
		for (d = -db / 2; d < db / 2; d += di)
		{
			mean[*nseg] = d;
			int nsegp = *nseg + 1;
			iterate_diam (geom, &nsegp); 
		}
	}
	else
	{
		geo* geom2 = geo_copy (geom);
		int s;
		for (s = 1; s < ns + 1; s++)
		{
			seg *segm2 = geo_get_nth (geom2, s);
			segm2->d0 += mean[s-1];
			if (s != ns)
				segm2->d1 += mean[s];
			// diameter cannot be bigger as bell end diameter!
			if (segm2->d0 > d1)
				segm2->d0 = d1;
			if (segm2->d1 > d1)
				segm2->d1 = d1;
		}

		fft *ffts = geo_fft (geom2, 400, 1.0, 0);
		peak *peaks = fft_peak (ffts, 1); // 1 = ground tone
		plugin_return *res = plugin_run (rank_module, (void *) peaks);
		int status = res->params[0].d_int;
		double rank = (double) res->params[1].d_float;
		peak_free (peaks);
		fft_free (ffts);

		if (status && (rank > best_rank))
		{
			best_rank = rank;
			if (best_geo)
				geo_free (best_geo);
			best_geo = geom2;
		}
		else
		{
			geo_free (geom2);		
		}
		plugin_return_free (res);
	}
}

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	name = PLUGIN_INFO.params[0].d_string;
	l = PLUGIN_INFO.params[1].d_float;
	ns = PLUGIN_INFO.params[2].d_int;
	d0 = PLUGIN_INFO.params[3].d_float;
	d1 = PLUGIN_INFO.params[4].d_float;
	s0 = PLUGIN_INFO.params[5].d_float;
	si = PLUGIN_INFO.params[6].d_float;
	db = PLUGIN_INFO.params[7].d_float;
	di = PLUGIN_INFO.params[8].d_float;

	rank_module = plugin_new (PLUGIN_INFO.params[9].d_plugin);
	plugin_init (rank_module, argc - 10, &(argv[10]));

	evpool = g_queue_new ();
}

void
deinit ()
{
	plugin_deinit (rank_module);
	plugin_free (rank_module);

	g_queue_free (evpool);
}

plugin_return *
run (void *i_data)
{
	mean = g_new (double, ns+1);
	best_rank = 0;
	iterate_seg (0, (double) 0.0);
	int s = 0;
	best_rank = 0;
	g_queue_foreach (evpool, iterate_diam, &s);

	if (best_geo)
	{
		char path[256];
		printf ("rank: %f\n", (double) best_rank);
		sprintf (path, "%s.eog", name);
			geo_eog (best_geo, path);
		sprintf (path, "%s.geo", name);
			geo_save (best_geo, path);
		fft *best_fft = geo_fft (best_geo, 800, 1.0, 0);
		sprintf (path, "%s.fft", name);
			fft_save (best_fft, path);
		peak *best_peak = fft_peak (best_fft, 1);
		sprintf (path, "%s.peak", name);
			peak_save (best_peak, path);

		peak_free (best_peak);
		geo_free (best_geo);
		fft_free (best_fft);
	}
	else
		printf ("no geometry meets your requirements\n");

	g_free (mean);

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (1);
	ret->types[0] = T_STATUS;
	ret->params[0].d_int = 0;

	return ret;
}

