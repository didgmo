/** \defgroup list list
 *
 * \ingroup modules
 */

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

#include "didgmo.h"

static plugin_param_type types [] = {
};

static plugin_param_def defs [] = {
};

static plugin_param params [0];

static plugin_info PLUGIN_INFO = {
	"list",
	"lists all available plugins",
	0,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
	PLUGIN_PARSE
}

void
deinit ()
{}

static int is_so (const struct dirent *d)
{
	return strstr (d->d_name, ".so");
}

plugin_return *
run (void *i_data)
{
	struct dirent **namelist;
	int num = scandir (PKGMODULESDIR, &namelist, is_so, alphasort);

	int i;
	for (i = 0; i < num; i++)
		printf ("%d %s\n", i, namelist[i]->d_name);

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (1);
	ret->types[0] = T_STATUS;
	ret->params[0].d_int = 0;

	return ret;
}

