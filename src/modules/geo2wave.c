/** \defgroup geo2wave geo2wave
 *
 * @param name name of modelling project
 * @param rate sample rate frequency (e.g 44100Hz)
 * @param duration duration of sound file in seconds
 *
 * \ingroup modules
 */

#include "didgmo.h"

static char *i_name;
static int i_rate;
static int i_dur;

static plugin_param_type types [] = {
	T_STRING,
	T_INT,
	T_INT
};

static plugin_param_def defs [] = {
	"modelling project name",
	"sample rate",
	"duration"
};

static plugin_param params [3];

static plugin_info PLUGIN_INFO = {
	"geo2wave",
	"simulate fft spectra based on a given geometry and fourier transform it into sound file",
	3,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	i_name = PLUGIN_INFO.params[0].d_string;
	i_rate = PLUGIN_INFO.params[1].d_int;
	i_dur = PLUGIN_INFO.params[2].d_int;
}

void
deinit ()
{}

plugin_return *
run (void *i_data)
{
	char path[256];
	sprintf (path, "%s.geo", i_name);
	geo *i_geo = geo_load (path);

	double i_max = 1000;
	double i_step = i_max / i_rate / i_dur;
	fft *o_fft = geo_fft (i_geo, i_max, i_step, 0);

	wave *o_wave = fft_wave (o_fft, 44100);

	sprintf (path, "%s.wav", i_name);
	wave_save (o_wave, path);

	wave_free (o_wave);
	fft_free (o_fft);
	geo_free (i_geo);

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (1);
	ret->types[0] = T_STATUS;
	ret->params[0].d_int = 0;

	return ret;
}

