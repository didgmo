/** \defgroup eog2geo eog2geo
 *
 * @param name name of modelling project
 *
 * @return NULL
 *
 * \ingroup modules
 */

#include <stdio.h>
#include <stdlib.h>

#include "didgmo.h"

static char *i_name;

static plugin_param_type types [] = {
	T_STRING
};

static plugin_param_def defs [] = {
	"modelling project name",
};

static plugin_param params [1];

static plugin_info PLUGIN_INFO = {
	"eog2geo",
	"converts a *.eog to an *.geo file",
	1,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	i_name = PLUGIN_INFO.params[0].d_string;
}

void
deinit ()
{}

plugin_return *
run (void *i_data)
{
	char path[256];
	sprintf (path, "%s.eog", i_name);
	geo *i_geo = eog_load (path);

	sprintf (path, "%s.geo", i_name);
	geo_save (i_geo, path);

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (1);
	ret->types[0] = T_STATUS;
	ret->params[0].d_int = 0;

	return ret;
}

