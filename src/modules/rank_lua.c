/* TODO
 * implementation!!
 */

/** \defgroup rank_lua rank_lua
 *
 * @param path path to lua description file
 *
 * @return the actual ranking
 *
 * \ingroup modules
 */

#include <stdio.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include "didgmo.h"

static char *path;
static lua_State *L;

static plugin_param_type types [] = {
	T_STRING
};

static plugin_param_def defs [] = {
	"path to lua description file",
};

static plugin_param params [1];

static plugin_info PLUGIN_INFO = {
	"rank_lua",
	"rank peaks according to a lua description file",
	1,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	path = PLUGIN_INFO.params[0].d_string;

	L = luaL_newstate ();
	luaL_openlibs (L);

	/*
	if (luaL_dofile (L, path))
		printf ("lua load error: %s\n", lua_tostring (L, -1));
	*/

	int i;
	lua_newtable (L); // arg
	for (i = 1; i < argc; i++)
	{
		lua_pushstring (L, argv[i]);
		lua_rawseti (L, -2, i);
	}
	lua_setglobal (L, "arg");

	if (luaL_dofile (L, path))
		printf ("lua parse error: %s\n", lua_tostring (L, -1));
}

void
deinit ()
{
	lua_close (L);
}

plugin_return *
run (void *p_peak)
{
	peak *i_peak = (peak *) p_peak;

	int i;
	double res = 0.0;

	lua_getglobal (L, "rank");
	if (!lua_isnil (L, -1))
	{
		lua_newtable (L); // peak

		lua_newtable (L); // min_freq
		for (i = 0; i < i_peak->min_size; i++)
		{
			lua_pushnumber (L, i_peak->min_freq[i]);
			lua_rawseti (L, -2, i + 1);
		}
		lua_setfield (L, -2, "min_freq");

		lua_newtable (L); // max_freq
		for (i = 0; i < i_peak->max_size; i++)
		{
			lua_pushnumber (L, i_peak->max_freq[i]);
			lua_rawseti (L, -2, i + 1);
		}
		lua_setfield (L, -2, "max_freq");

		lua_newtable (L); // min_amp
		for (i = 0; i < i_peak->min_size; i++)
		{
			lua_pushnumber (L, i_peak->min_amp[i]);
			lua_rawseti (L, -2, i + 1);
		}
		lua_setfield (L, -2, "min_amp");

		lua_newtable (L); // max_amp
		for (i = 0; i < i_peak->max_size; i++)
		{
			lua_pushnumber (L, i_peak->max_amp[i]);
			lua_rawseti (L, -2, i + 1);
		}
		lua_setfield (L, -2, "max_amp");

		if (lua_pcall (L, 1, 1, 0))
			printf ("lua run error: %s\n", lua_tostring (L, -1));
		res = (double) lua_tonumber (L, -1);
	}
	lua_pop(L, 1);

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (2);
	ret->types[0] = T_STATUS;
	ret->types[1] = T_FLOAT;
	ret->params[0].d_int = 0;
	ret->params[1].d_float = (double) res;

	return ret;
}

