/** \defgroup rank_gradual rank_gradual
 *
 * @param freq_low lower limit of frequencies
 * @param freq_high higher limit of frequencies
 * @param num_peak number of peaks that shall be optimized
 *
 * @return the actual ranking
 *
 * \ingroup modules
 */

#include <stdio.h>
#include "didgmo.h"

static int freq_low_band;
static int freq_high_band;
static double diff_ground;
static int num_peak;

static plugin_param_type types [] = {
	T_INT,
	T_INT,
	T_INT
};

static plugin_param_def defs [] = {
	"frequency low band",
	"frequency high band",
	"number of peaks that shall be optimized"
};

static plugin_param params [3];

static plugin_info PLUGIN_INFO = {
	"rank_gradual",
	"rank given number of peaks according to ground tone frequency band and relative peak heights",
	3,
	types,
	defs,
	params
};

PLUGIN_REGISTER

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	freq_low_band = PLUGIN_INFO.params[0].d_int;
	freq_high_band = PLUGIN_INFO.params[1].d_int;
	num_peak = PLUGIN_INFO.params[2].d_int;
}

void
deinit ()
{

}

plugin_return *
run (void *p_peak)
{
	peak *i_peak = (peak *) p_peak;

	double res;

	if ( (i_peak->max_freq[0] < freq_low_band)
		|| (i_peak->max_freq[0] > freq_high_band))
		res = 1e4;
	else
		res = 1e5;

	int p;
	double rel_peak [i_peak->max_size];
	for (p = 0; p < i_peak->max_size; p++)
		rel_peak[p] = i_peak->max_freq[p] / i_peak->max_freq[0];

	// bubble sort
	int i,j;
	for (i = 0; i < i_peak->max_size; i++)
		for (j = i; j > i_peak->max_size; j++)
		{
			if (rel_peak[i] < rel_peak[j])
			{
				double tmp = rel_peak[i];
				rel_peak[i] = rel_peak[j];
				rel_peak[j] = tmp;
			}
		}

	for (p = 0; p < num_peak; p++)
		res *= rel_peak[p];

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (2);
	ret->types[0] = T_STATUS;
	ret->types[1] = T_FLOAT;
	ret->params[0].d_int = 0;
	ret->params[1].d_float = (double) res;

	return ret;
}

