/** \defgroug sensitivity sensitivity
 *
 * starts a graphical user interface for interactive sensitivity analysis
 * @param i_name project name
 *
 * \ingroup modules
 */

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h> // mkfifo

#include "didgmo.h"

static plugin_param_type types [] = {
	T_STRING
};

static plugin_param_def defs [] = {
	"project name"
};

static plugin_param params [1];

static plugin_info PLUGIN_INFO = {
	"sensitivity",
	"interactive sensitivity analysis",
	1,
	types,
	defs,
	params
};

PLUGIN_REGISTER

static char *i_name;
static char CMD [256];
static FILE *fifo;

void
init (int argc, char **argv)
{
	PLUGIN_PARSE

	i_name = PLUGIN_INFO.params[0].d_string;

	gtk_init (&argc, &argv);

	sprintf (CMD, "%s.fifo", i_name);
	mkfifo (CMD, S_IRUSR | S_IWUSR);

	sprintf (CMD, "gnuplot -persist < %s.fifo &", i_name),
	system (CMD);

	sprintf (CMD, "%s.fifo", i_name);
	fifo = fopen (CMD, "w");
}

void
deinit ()
{
	fprintf (fifo, "quit ()\n", i_name),
	fflush (fifo);
	fclose (fifo);

	sprintf (CMD, "%s.fifo", i_name);
	remove (CMD);
}

plugin_return *
run (void *i_data)
{
	GtkWidget *window;
	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *spin;
	GtkWidget *vscale;
	GtkWidget *hscale;

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width (GTK_CONTAINER (window), 10);
	gtk_widget_show (window);

	hbox = gtk_hbox_new (1, 10);
	gtk_container_add (GTK_CONTAINER (window), hbox);
	gtk_widget_show (hbox);

	vscale = gtk_vscale_new_with_range (0.0, 0.2, 0.01);
	gtk_container_add (GTK_CONTAINER (hbox), vscale);
	gtk_widget_show (vscale);

	vbox = gtk_vbox_new (1, 10);
	gtk_container_add (GTK_CONTAINER (hbox), vbox);
	gtk_widget_show (vbox);

	spin = gtk_spin_button_new_with_range (0, 10, 1);
	gtk_container_add (GTK_CONTAINER (vbox), spin);
	gtk_widget_show (spin);

	hscale = gtk_hscale_new_with_range (0.0, 2.0, 0.01);
	gtk_container_add (GTK_CONTAINER (vbox), hscale);
	gtk_widget_show (hscale);

	gtk_main ();	

	fprintf (fifo, "call '%s/template.gpi' '%s'\n", PKGSYSCONFDIR, i_name),
	fflush (fifo);

	/*
	 * return object
	 */
	plugin_return *ret = plugin_return_new (1);
	ret->types[0] = T_STATUS;
	ret->params[0].d_int = 0;

	return ret;
}

